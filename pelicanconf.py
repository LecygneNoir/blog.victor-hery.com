#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = 'Victor'
AUTHORS = {
    "Victor": {
        "url": "https://blog.victor-hery.com",
        "blurb": "est le rédacteur principal du blog.",
        "avatar": "/images/authors/face.png",
    }
}
# SITENAME is on top of page
SITENAME = 'Blog de Victor Héry'
# SITESUBTITLE add a "SITENAME - SITESUBTITLE" line in footer
SITESUBTITLE = ''
SITEURL = ''
DEFAULT_CATEGORY = 'Système'
USE_FOLDER_AS_CATEGORY = False
THEME = './theme'
DIRECT_TEMPLATES = (('index', 'tags', 'categories','archives', 'search', '404'))

SITE_LICENSE='Ce blog est disponible sous licence <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.fr">Creative Commons attribution, partage dans les mêmes conditions</a>, merci :-)'

LANDING_PAGE_TITLE = "Je blog réseaux, système et jardins."

PATH = 'content'
STATIC_PATHS = ['images']
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
SUMMARY_MAX_LENGTH = 50
DEFAULT_PAGINATION = 4

### Plugins ###
PLUGIN_PATHS = ['../pelican-plugins']
PLUGINS = ['pelican_comment_system', 'extract_toc', 'sitemap', 'tipue_search']

#sitemap
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# Comments
PELICAN_COMMENT_SYSTEM = True
PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('author',)
PELICAN_COMMENT_SYSTEM_DIR = 'comments'
PELICAN_COMMENT_SYSTEM_AUTHORS = {
    ('Victor',): "images/authors/face.png",
    ('victor',): "images/authors/face.png",
    ('victorhery',): "images/authors/face.png",
}
COMMENT_URL = "#comment_{slug}"
COMMENTS_INTRO = "Si vous avez des questions, si quelque chose n'est pas clair, n'hésitez pas à commenter !"

#Markdown PLUGINS
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = 'feeds/all.atom.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Blog statique avec Pelican', 'http://getpelican.com/'),
         ('Un blog où l\'on parle d\'openvz, de QOS et d\'auto-hébergement', 'http://blog.developpeur-neurasthenique.fr/'),
         ('Projets OpenVZ-diff-backups', 'http://projets.developpeur-neurasthenique.fr/projects/openvz-diff-backups/'),
         ('Thème graphique Elegant', 'http://oncrashreboot.com/elegant-best-pelican-theme-features'),)

# Social widget
SOCIAL = (
    ("RSS", SITEURL + "/feeds/all.atom.xml"),
    ("git", "https://git.lecygnenoir.info/LecygneNoir/blog.victor-hery.com"),
    ("keybase", "https://keybase.io/victorhery"),
    ("mastodon", "https://mamot.fr/@victorhery"),
    ("Twitter", "https://twitter.com/victorhery"),
)
SOCIAL_PROFILE_LABEL = "Restez en contact"


DEFAULT_METADATA = {
    'status': 'draft',
    'lang': 'fr',
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
