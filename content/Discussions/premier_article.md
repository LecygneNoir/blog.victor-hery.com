Title: Premier article...
Date: 2012-08-12 10:20
Modified: 2012-08-12 19:30
Category: Discussion
Tags: introduction
Slug: premier-article
Authors: Victor
Status: published


... car il en faut bien un, s'pas ?



Bonjour, à tous et bienvenue.

Finalement, je me décide à créer un blog, après quelques autres tentatives plus ou moins fructueuses pour écrire et partager des tutoriels sur des choses qui m'intéressent.

J'espère que ce blog ne mourra pas dans l'oeuf (ça reste une option à ne pas ignorer smiley). En tout cas, je tâcherai de poster des articles et des tutos sur les sujets globaux de systèmes et réseaux.

(Effectivement ça rime)



Si je me sens en forme d'ailleurs, je rédigerai mes postes en alexandrins (parce que c'est marrant).

Plus sérieusement, je tâcherai de faire en sorte de ne pas m'ennuyer ici, et du coup que mes visiteurs en fassent de même !



Bonne journée, et bonne lecture.
