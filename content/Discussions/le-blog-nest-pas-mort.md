Title: Non le blog n'est pas mort :)
Date: 2015-07-12 09:17
Modified: 2015-07-12 09:17
Category: Discussion
Tags:
keywords:
Slug: le-blog-nest-pas-mort
Authors: Victor
Status: published

<p>Bonjour à tous,</p>

<p>&nbsp;</p>

<p>Comme le dit si bien le titre, non le blog n'est pas mort.</p>

<p>Bon, il est pas non plus hyper vivant je me fais pas d'illusions...</p>


<p>J'ai malheureusement (à ma grande honte) disposé et débloqué très peu de temps pour m'en occuper.</p>

<p>Du coup, il y a plusieurs brouillons dans les tuyaux, pas mal d'idées, des articles pas finis, ...</p>

<p>&nbsp;</p>

<p>J'ai conscience que c'est un peu frustrant, et je vois régulièrement des commentaires qui montrent que le blog est toujours lu.</p>

<p>En attendant d'avoir le temps de faire plus (j'y compte bien, sisi !), je vous annonce la mise en place d'une page "Contact".</p>

<p>Disponible en haut du blog parmi les sections (Articles, Présentation, ...), cela vous permettra de m'envoyer directement un courriel si vous souhaitez discuter d'un article, poser une question, avancer de votre côté sur un quelconque problème levé par un article, etc.</p>

<p>&nbsp;</p>

<p>L'idée étant principalement que le système de commentaire est assez vite limité pour les échanges vraiment constructifs et les questions qui y sont parfois posées sont compliquées à répondre dans un si petit espace.</p>

<p>Un courriel permettra d'initier un vrai dialogue ! ;)</p>

<p>N'hésitez donc pas à me contacter sur ce formulaire de contact (avec une adresse valable si vous souhaitez que je vous réponde \o/ ) et je reviendrai vers vous avec grand plaisir !</p>

<p>A très bientôt à tous, merci pour votre soutien et vos différents commentaires sur mes articles !</p>

<p>Victor</p>
