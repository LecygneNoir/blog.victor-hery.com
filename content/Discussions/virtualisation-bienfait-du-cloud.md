Title: La virtualisation, ou l'un des (seul ?) bienfait du cloud
Date: 2012-08-15 17:51
Modified: 2012-08-15 17:51
Category: Discussion
Tags: linux, debian, virtualisation
keywords: linux, debian, virtualisation
Slug: virtualisation-bienfait-du-cloud
Authors: Victor
Status: published

[TOC]

## La virtualisation, qu'est ce que c'est ?

<p>La virtualisation de serveur, ça consiste à se servir d'une machine physique pour émuler plusieurs machines dites 'virtuelles'. La machine physique, autrement nommée <a href="http://fr.wikipedia.org/wiki/Hyperviseur">Hyperviseur</a>, va permettre de gérer de A à Z les machines virtuelles hébergées.</p>

<p>Gestion processeur, RAM, espace disque, réseau, extinction, ... l'hyperviseur à la main sur tout ce qui à trait à ses machines virtuelles (VM pour Virtual Machine)</p>

<p>Chaque machine virtuelle par contre, vu de l'intérieur, est autonome. On la démarre, et le système à l'intérieur fonctionne tout seul, sans intervention extérieure. De son point de vue, elle est une machine physique. Elle dispose d'une configuration propre (définie par l'hyperviseur, vous suivez ?), et elle n'a pas "conscience" d'être en fait un sous ensemble d'une plus grosse machine physique.</p>

<p>Bien sûr, chaque hyperviseur a ses limites, il n'est pas possible d'installer trop de machines sur un serveur physique. La RAM que vous allouez par exemple à vos VM ne peut pas dépasser la RAM totale de l'hyperviseur, etc.</p>

<p>&nbsp;</p>

<p>Bref, ça a l'air super sexy, mais enfin bon, quelle utilité ?</p>

<p>&nbsp;</p>

## De l'utilité de la virtualisation

<p>D'abord, une constatation évidente. De nos jours, le moindre serveur dédié que vous pouvez louer est surdimensionné (à moins que vous ne souhaitiez vraiment faire tourner 70 sites web à vous tout seul, associé à 10000 adresses courriels et une 30aine de serveurs Counter Strike<sup>©</sup></p>

<p>Tout ça tournant bien sûr en vrac sur le serveur. Et si tout plante, et ben c'est con, mais y'a plus rien dis donc.</p>

<p>Sinon, vous avez une machine énorme pour faire tourner votre site web, et si vous voulez partager un peu vos services, ben vous achetez une deuxième machine énorme pour faire tourner votre serveur Teamspeak et votre Counter Strike<sup>©</sup></p>

### Séparons les services ! Tous les oeufs ne vont pas dans le même panier

<p>Le premier avantage à virtualiser, c'est que vous pouvez créer autant de machine virtuelle que de services que vous souhaitez faire tourner, par exemple.</p>

<p>Ainsi, une machine pour vos sites web, une machine pour votre serveur courriel, une machine pour les serveurs de jeu, etc. De cette façon, vous capitalisez un seul serveur physique pour tous vos services, sans que ceux-ci ne se marchent dessus !</p>

<p>Et en plus, ça vous permet de répartir exactement quelle ressource physique (processeur, RAM, ...) vous voulez allouer à vos services.</p>

<p>Un pirate attaque votre serveur web en saturant la RAM ? Pas grave, seul les petits 512Mo que vous lui aviez alloué vont se remplir. La VM va planter, les sites web devenir inaccessibles, mais pendant ce temps tout le reste continuera à tourner.</p>

<p>D'ailleurs, parlons sécurité.</p>

### Séparons les services ! bis. Le panier, il fuit pas

<p>Et oui, l'un des autres gros avantages, c'est que si la machine qui contient votre serveur web (imaginons une faille dans apache par exemple), est compromise par un pirate, et bien, seule cette machine est compromise.</p>

<p>Comme de son point de vue elle est une machine physique à part entière, elle n'a pas de moyen "physique" de joindre les autres machines virtuelles, sauf par le réseau classique.</p>

<p>Du coup, le pirate peut effectivement s'amuser avec les sites web (ça, on ne peut malheureusement rien y faire), mais même s'il sature la mémoire, remplit le disque dur, surcharge le processeur, attaque la base mysql ou que sais-je d'autre, ça restera confiné à cette machine. Et les limitations mises en place par l'hyperviseur feront que ces dommages resteront minimes du point de vue des autres machines.</p>

<p>De plus, dans l'idéal le pirate n'a aucun moyen de savoir que cette machine est en réalité une machine virtuelle, il n'est donc pas tenté d'attaquer le reste des services. (Enfin, à la limitation que ces services ne soient pas visibles sur Internet. Sinon à moins d'être débile, notre pirate aura au moins des doutes.)</p>

<p>&nbsp;</p>

### Limitons les services : moins y'a de portes, plus c'est dur d'entrer

<p>Bien sûr, l'hyperviseur lui-même est le gros maillon faible de l'infrastructure. Si lui se trouvait être compromis, alors toutes les machines virtuelles seraient potentiellement en danger. Mais en y réfléchissant, ça serait pareil si tout tournait sur le serveur physique. S'il était compromis, tout serait compromis. Et en plus, il y aurait plus de porte pour y entrer.</p>

<p>En effet, chaque service (courriel, web, serveur de jeu) ouvert sur Internet est une porte potentielle pour un pirate, qui peut essayer de la forcer pour entrer sur votre serveur et mettre la pagaille.</p>

<p>Là, comme en théorie l'hyperviseur ne fait tourner aucun service dit sensible, il peut très bien être complètement invisible depuis Internet ! Et moins il y a de portes, plus c'est facile de les surveiller. Chaque machine peut avoir un bon gros pare-feu qui n'autorise que son service, limitant ainsi beaucoup les effets de bord.</p>

<p>Pour que ça soit encore plus cool, la plupart des hyperviseur implémentent un moyen de se connecter sur ses machines virtuelles. En gros, connectez vous sur l'hyperviseur, vous pourrez vous connecter sur toutes les machines. Ca peut sembler evident, mais ça permet du coup de se passer de tout accès SSH, telnet, rdp, ... sur les machines. Donc, d'autant de services offrant des opportunités d'entrer pour un pirate&nbsp; :-) </p>

<p>&nbsp;</p>

### Groupons les sauvegardes et migrons les machines

<p>Un autre point sympathique de la virtualisation, c'est la gestion des sauvegardes. Tout hyperviseur digne de ce nom vous permet de sauvegarder l'intégralité d'une VM.&nbsp;</p>

<p>Il est ainsi extremement simple de sauvegarder toutes vos machines virtuelles, et de redonder ces sauvegardes pour que même en cas de coupure de courant, de disque qui lâche ou de guerre nucléaire, vous ayez tout ce qu'il faut sous le tapis.</p>

<p>Et en plus, les machines virtuelles ont le bon goût de ne pas trop se soucier de l'hyperviseur qui les fait tourner. C'est à dire que en cas de coupure de courant, de disque qui lâche ou de guerre nucléaire, vous prenez votre petite sauvegarde, un autre serveur avec l'hyperviseur installé, et hop, toutes vos machines redémarrent !</p>

<p>Au prix éventuellement d'une petite reconfiguration d'adresses IP et de serveurs DNS, tous vos services peuvent être disponible de nouveau en un temps très court. Et plus court encore si vous avez prévu le coup et avez déjà une copie de votre hyperviseur prête à l'emploi à tout moment.</p>

<p>&nbsp;</p>

## J'y crois pas, y'a forcément un mais

<p>Oui bon ok, il y a aussi des points négatifs.</p>

### Accès à l'hyperviseur

<p>L'un d'eux, c'est que si vous voulez accéder à votre hyperviseur pour ensuite accéder à vos machines (et ainsi vous affranchir de l'accès à vos machines une par une), il faut ouvrir ce service sur l'hyperviseur. Eventuellement depuis Internet.</p>

<p>Bon, dans le cas d'un serveur sous Windows, je ne vous cacherai pas qu'un vieux Remote Desktop Server bien pas sécurisé, bien moche et bien connu du moindre bébé pirate sorti du ventre de sa mère, ça pue. Ca pue vraiment. Laisser votre hyperviseur avec "ça" ouvert sur le net, c'est se tirer une balle dans la tête (non non, même pas dans le pied, dans la tête)</p>

<p>Après, si vous êtes quelqu'un de sérieux (troll inside) et que vous faites vos serveurs sous Linux/Unix, laisser un accès SSH ouvert, c'est acceptable. SSH est bien connu, très régulièrement mis à jour, facile à sécuriser. Je peux d'ailleurs vous conseiller un petit <a href="http://www.fail2ban.org/wiki/index.php/Main_Page">fail2ban</a> à cet effet, simple et très efficace.</p>

<p>Là, on est propre, et on peut se connecter assez sereinement à notre hyperviseur.</p>

<p>&nbsp;</p>

### La taille des sauvegarde

<p>Malheureusement ça, c'est un fait. Sauvegarder des machines entières, ça prend de la place. Là où avant vous sauvegardiez un dossier avec vos sites, deux ou trois fichier de conf et une base de données, vous devez maintenant sauvegarder plusieurs serveurs. Chacun embarquant son système d'exploitation à lui. Et là, ça peut vite faire mal niveau espace disque nécessaire pour tout sauvegarder.</p>

<p>Mais bon, au moins vos machine sauvegardées peuvent repartir à tout moment. C'est déjà ça, hein ?</p>

<p>&nbsp;</p>

### Les mises à jour - Aie !

<p>Mais le vrai gros point noir de la virtualisation d'après moi, c'est la gestion des mises à jour.</p>

<p>En effet, si vos machines sont accessibles depuis le net, elles DOIVENT être à jour. On ne reviendra pas là dessus.</p>

<p>Cependant, avoir 4 machines qui tournent, c'est devoir mettre 4 machines à jour. L'hyperviseur, la VM du serveur web, la VM du serveur courriel et la VM de votre serveur de jeu doivent être mise à jour séparement.</p>

<p>Et ça, ça fait mal. C'est chronophage. C'est redondant. Ca pique. Et ça ennuie...</p>

<p>On peut tenter du script de mise à jour automatique, toutes les nuits. On lance la mise à jour, on répond automatiquement oui à toutes les questions, et c'est finit. Oui, jusqu'à ce qu'un fichier de conf soit changé durant la mise à jour, jusqu'à ce qu'un nouveau paramètre fasse son apparition, jusqu'à ce que... Bref, c'est prendre le risque qu'en vous levant un matin, plus rien ne fonctionne, et que vous deviez restaurer une sauvegarde de la veille faute de trouver qu'est ce qui a exactement été changé...</p>

<p>Donc non, mise à jour à la main, et une par une.</p>

<p>Et ça, malgré mes diverses recherches, je n'ai pas encore réussit à m'en affranchir dans le monde de la virtualisation...</p>

<p>&nbsp;</p>

## Mais bon, c'est trop bien quand même !

<p>Oui, parce qu'avec tous les avantages, ça serait quand même dommage de finir sur une note négative !</p>

<p>Pour ma part, je virtualise exclusivement sous debian, et j'utilise un outil nommé <a href="http://www.proxmox.com/">proxmox</a>. D'ailleurs, je prévoie un petit article à ce sujet bientôt&nbsp;:-)</p>

<p>EDIT : article rédigé <a href="/2012/09/virtualisation-opensource-qui-roxx-proxmox.html">ici</a>.</p>
