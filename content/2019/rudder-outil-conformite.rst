Suivre vos serveurs avec rudder
###############################
:subtitle: \- conformité et maîtrise opérationnelle
:date: 2019-07-28 18:32
:authors: Victor
:slug: rudder-outil-conformite
:category: Système
:tags: rudder, mco, conformité, ansible
:keywords: rudder, mco, conformité, ansible
:status: published
:summary: conformité et maîtrise opérationnelle

.. contents::

La conformité, késako ?
=======================

Ce que l'on appelle la conformité dans le monde merveilleux des sysadmins, c'est ce qui va vous permettre de maintenir et de faire fonctionner vos serveurs sur le long terme.

Parmis les étapes les plus connues de la vie d'un serveur, il y a son installation, sa configuration puis son utilisation (serveur web, applicatif, ...).

**La conformité va intervenir juste après l'installation du serveur.** Ce n'est pas du déploiement, ni de la supervision, c'est ce qui va vous permettre  de vous assurer que votre serveur ne dérive pas, que sa configuration reste correcte et qu'il continue de ressembler à ce qu'il doit être.

**Note :** Dans le monde merveilleux des DevOps, vous entendrez aussi parler de Maintenance en Conditions Opérationnelles (MCO).

Différents outils existent qui font plus ou moins ce travail, comme puppet dont les agents permettent de faire des modifications dans la vie du serveur, ou Ansible dont on peut rejouer les playbooks pour recaler un serveur qui aurait divergé.

Cela étant, peu d'outils sont spécifiquement dédiés à ce besoin, et c'est le cas de `rudder
<https://www.rudder.io>`_.

.. figure:: {static}/images/rudder-outil-conformite/node_breakdown.png
    :width: 700px
    :alt: Quelques informations d'inventaire

    De jolis camemberts d'inventaires

L'idée est simple : vous définissez des règles que vos serveurs doivent suivre, et rudder s'assure qu'ils s'y conforment, détecte les changements et les corrige le cas échéant.

Si vous avez besoin que votre **motd contienne des infos légales** et de vous assurez qu'elles sont toujours présentes. Si vous voulez être sûr que **la connexion SSH root est désactivée et le reste**. Si vous voulez pouvoir **déployer des clefs SSH autorisées** à se connecter au serveur et surveiller que personne n'en ajoute de nouvelles dans votre dos. Ou encore **pouvoir massivement ajouter et supprimer des utilisateurs systèmes** quand quelqu'un quitte ou rejoint la boite.

Alors vous avez besoin de conformité, et rudder est fait pour vous !

Ce schéma définit bien où se situe rudder dans les différentes étapes d'exploitation :

.. figure:: {static}/images/rudder-outil-conformite/schema-rudder-build-run.png
    :width: 700px
    :alt: rudder en exploitation

    Rudder, outil géré par le Run

En espérant que le concept de conformité vous soit déjà un peu plus familier, passons à la suite !

Installons Rudder
=================

Rudder consiste en deux composants principaux. Le serveur, et les agents.

Serveur Rudder
--------------
Le server va compiler un nombre d'informations assez importantes, et notamment les différentes règles et techniques pour définir les ordres à envoyer aux agents.

Mieux vaut donc prévoir un serveur relativement costaud. 4Go de RAM me parait un minimum si vous compter gérer plus d'une 20aine de serveur.

En outre rudder conseille de séparer vos partitions, notamment /var où il écrira par défaut, car il peut générer beaucoup de données.

Lançons donc notre petite installation, pour du debian :

.. code-block:: bash

        wget --quiet -O- "https://repository.rudder.io/apt/rudder_apt_key.pub" | sudo apt-key add -
        echo "deb http://repository.rudder.io/apt/5.0/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/rudder.list
        apt-get update
        apt-get install rudder-server-root


Une fois l'installation terminée, rudder met à disposition un script pour permettre de configurer le serveur en répondant à quelques questions :

.. code-block:: bash

        /opt/rudder/bin/rudder-init

Notamment, pensez à configurer les réseaux qui seront **autorisés à se connecter** au serveur, surtout si vous compter l'utiliser sur des réseaux qui ne sont pas privés.

Finalement, le script vous fourni une URL où vous connecter à l'interface. Pour la première connexion, les identifiants sont tout simplement `admin / admin`

Inutile de dire que je vous recommande de les changer 😋

Votre serveur est prêt, vous pouvez commencer à vous balader !

Agents Rudder
-------------

Sans agent, le serveur ne vous sert pas à grand chose. Les agents sont les machines que vous souhaitez surveiller et qui viendront rendre des comptes au serveur.

Rudder fournit une `liste de compatibilité
<https://docs.rudder.io/reference/5.0/installation/operating_systems.html#_for_rudder_nodes>`_. plutôt importante. L'inscription au support payant permet de supporter des OS plus anciens ou exotiques, à vous de voir selon vos besoins.

Normalement de base votre serveur rudder devrait également être considéré comme un agent, après tout lui aussi doit être conforme.

Mais pour l'exemple, on va en installer un 2eme.

Sur une machine différente du serveur rudder :

.. code-block:: bash

        wget --quiet -O- "https://repository.rudder.io/apt/rudder_apt_key.pub" | sudo apt-key add -
        echo "deb http://repository.rudder.io/apt/5.0/ $(lsb_release -cs) main" > /etc/apt/sources.list.d/rudder.list
        apt-get update
        apt-get install rudder-agent

Une fois l'agent installé, il ne démarre pas directement.

Il va falloir lui spécifier où est son serveur via le fichier `/var/rudder/cfengine-community/policy_server.dat`. Indiquez tout simplement l'IP ou le hostname du serveur rudder.

Une fois cela fait vous pouvez le démarrer :

.. code-block:: bash

        rudder agent start

**Note :** cette commande est un wrapper pour les différents systèmes de gestion de services (initd, sysvinit, systemd, ...).
De plus pour éviter à un attaquant une désactivation trop facile, rudder installe diverses crons pour vérifier que l'agent tourne et le relancer si besoin.
Une fois lancé, il devient donc assez difficile de se débarrasser de l'agent sauf à le supprimer complètement 😉

Les règles
==========

Dans rudder, les **règles** sont l'état que vous souhaitez appliquer sur vos serveurs. Elles sont composées de **techniques** que vous pouvez combiner de différentes manières.

Il y en a une infinité selon vos besoins et votre imagination, mais la première chose à appréhender est que vous pouvez décider de les **rendre obligatoires, ou simplement surveiller**.

Audit et Enforce
----------------

Ces 2 possibilités se présentent sous les mots clés Audit et Enforce.

En mode **Audit**, une règle se contentera de comparer ses ordres avec l'état réel du système, et de remonter les différences.
Cela vous permet notamment de **surveiller** l'état d'un serveur, voir s'il change de configuration, tester vos techniques, etc.

En mode **Enforce** par contre, on est beaucoup plus rentre-dedans. Dans ce mode, si un système diverge des techniques qui doivent s'appliquer, alors rudder remontera toujours l'information, mais surtout il prendra la décision de **corriger** le problème.

Cela peut se traduire par des suppressions d'utilisateurs nouvellement créés, la modification des clefs SSH déployés, la reinitialisation de telle ou telle configuration sur le serveur, tout dépend de vos règles.

A l'installation, rudder déploie des règles en Enforce qui vont lui permettre de **gérer les agents** (vérifier la configuration de l'agent, le mettre à jour, s'asurer qu'il est lancé, ...).

Ces règles sont utiles notamment pour rendre plus difficile à un attaquant la désactivation de rudder.

.. figure:: {static}/images/rudder-outil-conformite/rudder_agent_basic.png
    :width: 500px
    :alt: Les règles de configuration de l'agent rudder

    Les règles de configuration de l'agent rudder

Par la suite vous allez pouvoir ajouter vos propres règles, et pouvoir choisir si vous souhaitez qu'elles fassent des actions (Enforce) ou se contentent de surveiller les problèmes (Audit).


Les Techniques
--------------

Les techniques sont la brique élémentaire qui va vous permettre de construire vos règles à appliquer. Rudder en contient un certain nombre de base, mais il existe également un éditeur qui vous permet de créer les vôtres de manière plutôt simple.

Voici quelques exemples de techniques existantes qui vous permettent de jouer un peu :

.. figure:: {static}/images/rudder-outil-conformite/technique_motd.png
    :width: 80%
    :alt: Une technique pour forcer un texte dans votre motd

    Une technique qui vous permet de forcer un texte dans votre motd

.. figure:: {static}/images/rudder-outil-conformite/technique_ssh.png
    :width: 80%
    :alt: Une technique pour vous assurer qu'une clef ssh est présente

    Une technique qui vous permet de vous assurer que votre clef ssh est présente pour la connexion

.. figure:: {static}/images/rudder-outil-conformite/technique_user.png
    :width: 80%
    :alt: Une technique pour vérifier qu'un utilisateur est bien créé

    Une technique qui vous permet de créer un utilisateur et de vous assurer qu'il reste présent

Ce sont là **quelques exemples**, mais là où cela devient vraiment puissant, c'est que ces techniques peuvent tout à fait se combiner.

Par exemple, rien ne vous empêche de vous assurer qu'un utilisateur "sauvegarde" existe sur vos serveur, et qu'il autorise la connexion avec la clef ssh spécifique de votre outil de backup.

Ou bien que l'utilisateur "sauvegarde" n'est présent que sur vos hyperviseurs, pour permettre de sauvegarder vos machines virtuelles.

Ou encore que le stagiaire dispose d'un compte sur les serveurs de développement, mais **jamais sur les serveurs de production**.

Tout en étant assuré que le motd contient bien les infos légales prévenant que la connexion n'est pas autorisée, et que personne n'a été le supprimer pour être blindé en cas d'audit.

Vous pouvez aussi déployer sur **l'intégralité de vos 3000 serveurs** la clef SSH de la nouvelle recrue, pourquoi pas avec des droits restreints, en moins de 5 minutes 😉


Enregistrer un serveur
======================

Pour éviter de vous retrouver à déployer vos règles n'importe où, rudder demande à ce que les nouveaux serveurs soient **enregistrés** avant de pouvoir être utilisés.

Il est possible de gérer cet enregistrement plus ou moins automatiquement, mais par défaut c'est manuel.

Cela signifie que dès qu'un serveur a son agent qui démarre, il va apparaître dans l'interface de rudder, et vous pourrez définir si oui ou non il est légitime, l'assigner à des groupes, lui indiquer des règles, etc.

Vous pouvez également définir des **templates** qui permettront de déployer directement des règles sur certains types de serveurs. Par exemple, s'il s'agit d'un hyperviseur ou d'une VM, s'il est formaté d'une certaine manière, s'il dispose d'un OS particulier, si un serveur web est installé, ...

Rien ne vous empêche donc de déployer votre serveur avec votre outil de déploiement préféré (ansible, salt, etc), lui installer sa suite logicielle nécessaire, puis laisser ensuite rudder ajouter les utilisateurs, configurer les sauvegardes, préparer tout le bousin légal, remonter un inventaire complet de l'intégralité du système.

Et en bonus, rudder vous assure que tout reste en l'état pendant la durée de vie du serveur et vous sors de jolis diagrammes en camembert pour vous montrer des aperçus de ce qui se passe 😄

Si vous souhaitez enregistrer en masse des serveurs existants, par exemple parce que vous déployez rudder sur votre infra existante, je vous conseille tout de même de configurer vos règles en **Audit** pour être sur de ne rien casser et vous permettre de faire un point sur l'état de l'infrastructure.

Conclusion
==========

J'ai présenté dans cet article les concepts de base de la compliance et de comment utiliser rudder pour permettre de répondre à cette problématique.

C'est un outil vraiment très pratique et qui laisse une grande part à l'imagination pour gérer votre infrastructure avec la souplesse de ses techniques, je l'utilise personnellement depuis presque 4 ans au quotidien et j'en suis vraiment très content.

Avoir un pool de serveurs à gérer n'a jamais été aussi agréable !

Si cet article vous a plu, ou si vous souhaitez que j'aborde plus en détails certains points de rudder, n'hésitez pas à laisser un commentaire !
