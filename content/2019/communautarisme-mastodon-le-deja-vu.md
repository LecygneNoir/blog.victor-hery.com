---
Title: Communautarisme sur Mastodon : le déjà-vu
date: 2019-07-09 03:26
Authors: Victor
Slug: communautarisme-mastodon-le-deja-vu
Category: Discussions
Tags: mastodon, communautarisme
keywords: mastodon, communautarisme
Status: draft
---

[TOC]

# Préambule
Et oui, bon je n'étais pas très inspiré pour le titre, c'est pas un sujet facile à résumer, alors voila.

Vu ce dont cet article va parler, un préambule s'impose.

**Edit** au 9 juillet à 18h
Merci pour tous vos retours sur ce sujet. Ils m'ont faire prendre conscience des différences d'interprétation qui existent autour du terme communautarisme. J'ai écris le préambule après le texte principale, dans une tentative d'amener du contexte, ce qui était une erreur.  
Certaines phrase à l'intérieur ne deviennent compréhensible qu'en ayant lu le reste du texte, ce qui est déconnant pour un préambule. Ça le rends inutilement brutal, ce qui n'était pas du tout mon objectif.  
Je l'ai donc édité pour repréciser ce que j'entends par le terme comunautarisme, et la différence fondamentale que j'y vois personnellement par rapport à la communauté. Je m'excuse pour ces tournures de phrases qui ont choquées.  
J'ai également modifié la partie sur la modération twitter. Par apolitique je voulais sous entendre que leur modération ne s'intéressait pas ou peu au propos et s'orientait sur le free speech à l'Américaine, très différent de notre liberté d'expression française. C'était maladroit, c'est corrigé.  
Je n'ai pas touché au reste du texte. Il est imparfait, il a été difficile à coucher à l'écrit, il ne prends pas en compte tous les cas et ne se veut ni exhaustif ni scientifique, je ne peux pas y prétendre et je n'essaie pas.
Mais il représente mon propre vécu et comme ont j'espère pu le voir ceux qui m'ont contacté à ce sujet, je suis totalement ouvert pour en discuter.  
Fin de l'edit.

Pour parler de communautarisme, il me faut parler de communauté. Une communauté est pour moi un regroupement de personne qui se sentent bien ensembles, qui partagent des choses qui valent la peine d'être vécues ensembles. Parfois il s'agit de se regrouper pour se défendre contre des agressions extérieures, parfois non. Ce premier cas est particulièrement vrai sur Mastodon, mais pas que.  
La communauté est pour moi un regroupement tout à fait sain, agréable et nécessaire, et je le différencie fondamentalement du communautarisme. 
 
**Je précise donc ** que je parle ici d'un phénomène qui est une **dérive** de la communauté. En général il se créé effectivement alors qu'une communauté existe déjà et je pense que c'est dommageable.  
C'est quelque chose qui n'arrive pas à toutes les communautés, je ne généralise rien, je tente juste de donner des clefs pour aider ceux qui le subissent à le comprendre ! Ce terme n'est peut pas pas adéquat, c'est possible mais je n'en ai pas d'autre.   
Des tas de fois (et bordel heureusement) **tout se passe très bien**. 

Certaines personnes qui liront ce texte se reconnaîtront.

 - Si vous vous reconnaissez dans les harcelés, lisez jusqu'au bout
 - Si vous vous reconnaissez dans les harceleurs, lisez jusqu'au bout. Posez vous la question. Pourquoi vous vous reconnaissez ?
 
J'ai tout à fait conscience avec cet article que je vais probablement me faire pourrir. Je l'assume. Les commentaires non constructifs, sans arguments à base de "tu peux pas comprendre" seront modérés. Je l'assume aussi.    
Ceux sur la base du "on peut plus rien dire, bravo la liberté d'expression" aussi.  
**Posez vous la question**, si vous ressentez le besoin d'écrire un commentaire vide de sens, c'est que cet article vous a touché. Mais que vous vous sentez du mauvais côté. Pourquoi ? **Réfléchissez-y**.

Cet article parle de communautarisme dans le sens d'une dérive de la communauté et non pas dans le sens de communauté. C'est pour moi le moment où le sentiment d'appartenance devient une raison d'exclusion plutôt que de regroupement. C'est souvent insidieux. Parfois à cause d'un effet de groupe, parfois à cause d'une personne.  
Parfois en toute bonne foi.  

Cet article est long. J'en suis désolé. Le problème est complexe, j'essaye de l'expliquer avec mes mots, mon expérience. C'est imparfait. Lisez en plusieurs fois si vous le souhaitez. Ou d'un coup, faites votre propre choix !

Je ne veux mépriser personne avec cet article. Je souhaite simplement poser les choses telles que je les ressens et que je les constate. Provoquer une discussion pourquoi pas, une prise de conscience peut-être, mais à aucun moment je ne souhaite ni critiquer.  
Les victimes du communautarisme tel que je l'entends sont des victimes, qu'elles soient dans le groupe ou qu'elles en subissent les effets, et c'est cette prise de conscience que j'essaye de provoquer avec ce texte.

Merci.

# Le phénomène

Ce que l'on constate sur Mastodon et twitter ces derniers jours (en juillet 2019) n'est pas un phénomène nouveau.  

Il existait avant ces réseaux, sur les forums. Et surtout avant ça, il existait et existe toujours IRL, dans des associations, des groupes, des partis.

Il existe très probablement depuis que le premier groupe d'homme s'est créé dans l'optique de **s'entraider** (le terme est important). A ce moment là, c'est parti en sucette.

Ce phénomène, c'est le communautarisme.   
Il est insidieux, il se glisse partout, parfois même à l'insu de ceux qui s'en font les garants. (oui, ça peut paraître difficile à croire)

Parce qu'il est destructeur, pour ceux qui le subissent comme souvent pour ceux qui y participent, j'ai voulu rédiger ce texte pour en parler, comme un exemple tiré de ma propre expérience.

C'est un support comme un autre, plus pratique à on sens, plus lisible à long terme qu'un thread sur les réseau sus-cités. Et puisqu'il peut s'appliquer à beaucoup d'autres lieux que ces réseaux, il me paraissait important d'en parler. Cf le préambule.

Je vais donc essayer ici de décrire ce phénomène à partir de mon expérience, et d'expliquer que non : vous n'êtes pas seuls. Surtout, surtout, faites attention à vous.  
Je vais parler ici à relativement petite échelle. Comment cela impacte une association, Mastodon, des structures relativement petites, relativement bon enfants, relativement artisanales. Il existe des études à propos de ce phénomène à plus grande échelle, une ville, un état, un pays. Je n'ai pas la prétention d'aller si loin ici.

Il n'y a ici **aucun procès d'intention.**

# Autour d'un exemple 

Bon, ceci étant posé, le communautarisme, d'où il vient, comment est ce qu'il arrive à se glisser, à pourrir un projet de l'intérieur ?

Prenons un exemple : quelques personnes se regroupent pour créer une structure, disons pour proposer à ceux qui le souhaite un coup de main, une oreille attentive, peut être un repas chaud, ou un local où se regrouper ou bien dormir quand on est dans la merde.

Ça se passe bien, il y a des rencontres, des échanges, l'idée se répand, le groupe grossis. À ce moment là, on est content de ce qu'on a accompli, on est content de venir en aide à d'autres, de savoir qu'à son échelle, on est capable de construire un truc.

Mais voila, c'est difficile de satisfaire tout le monde, de suivre la demande, et petit à petit, une pensée va commencer à s'imposer. Peut être que l'on devrait aider plutôt tel type de personnes. Elles l'ont "mérité", elles ont plus "souffert" que les autres, elles ont la priorité. (Notez que les personnes en question n'ont pas voix au chapitre en général. Si elles ont pas envie d'être aidées, tant pis pour elles c'est pas à elles de choisir)

C'est insidieux, ça peut venir de personnes présentes dans le groupe d'origine, ou de personnes l'ayant rejoint plus tard, mais petit à petit on va transformer le projet.  
À ce stade, on peut encore considérer que c'est normal. Après tout, impossible d'englober tout le monde dans un projet. Se spécialiser pour laisser le reste du combat à d'autres, ça s'entend, les ressources sont limitées, l'énergie aussi. Et puis, si quelqu'un dans la merde débarque, on va l'aider bien sûr, c'est normal.

C'est après que ça commence à devenir beaucoup plus vicieux.

Petit à petit, des gens vont laisser entendre qu'il faut faire plus, aller plus loin. On aide ceux qui l'ont mérités, les autres sont des connards. Et d'ailleurs, dans ce groupe, y'a des gens qui ont jamais souffert, qu'est ce qu'ils en savent de comment c'est ?  Et comment ils peuvent aider les autres, les comprendre, alors qu'ils ne font même pas partie de la communauté ?  
Si vous essayez de discuter, de débattre, de comprendre ce qui se passe, on ne fait que vous renvoyer l'étiquette : tu ne fais pas partie de cette communauté, ça ne te regarde pas, tu ne peux pas comprendre.

On vous fait comprendre que vous n'êtes plus le bienvenue. Tout ce que vous ferez ne suffira pas, vous ne faites pas partie des nôtres, vous ne pouvez pas comprendre.

Notez que les personnes qui initient ce mouvement peuvent très bien ne pas faire partie du tout de la communauté non plus. Ce n'est pas important, ce qui compte c'est le fait de pouvoir devenir le gourou de ceux qui les écoute. Et, oui, j'utilise totalement sciemment ce vocabulaire de secte.


C'est là, à ce moment précis, que le découragement et l'incompréhension arrivent.  
On se sent démuni, le projet part en sucette, des personnes qui avaient besoin d'aide se voient refuser l'entrée, voire même jetées dehors, parce qu'elles ne font pas partie de la communauté. Là encore, la communauté en question n'a pas voix au chapitre ! Elle peut même (c'est le cas sur Mastodon) être viscéralement contre ce genre de comportement, **ça n'a aucune importance, ça ne joue pas dans les réactions de ces gens.**


A ce moment, on passe de la communauté au communautarisme. Autour d'un ou plusieurs gourou(s) se forme un groupe, dissocié du projet d'origine, qui n'en partage plus du tout les valeurs, mais surtout qui ne s'en rends même pas compte, aveuglé par les belles paroles, des étoiles pleins les yeux quand le grand chef décrète que X ou Y n'a pas mérité leur considération. (mériter. Ce mot est décidément important. Surveillez le dans vos projets)

Alors vient le harcèlement, la stigmatisation, les agressions, les insultes. 

L'objectif est simple : tout faire pour décrédibiliser ceux qui menacent le gourou. Éliminer ce qu'ils voient comme une concurrence. Une concurrence pour lui, pas pour la communauté !  
Détruire, dégouter ceux qui ne partagent pas sa vision. Ou tout simplement profiter de son pouvoir en observant à quel point il peut, d'un claquement de doigt, littéralement détruire quelqu'un.  
Il n'y a pas de considération vis à vis des cibles, toutes sont bonnes à prendre, même, voire surtout, si elles font partie de la communauté. Après tout, elles ne sont pas suffisamment SI ou ÇA, sinon elles seraient avec nous, contre le reste du monde.

Cibler une figure de proue pour écoeurer les gens moins impliqués est une excellente stratégie, très efficace. En général, un des créateur du projet s'il en reste, ou quelqu'un qui se défonce pour le projet. C'est facile, vu comme il s'implique, c'est douteux, il ne peut qu'être un sale égoïste, ne pas comprendre ceux qui souffrent et ne peuvent pas donner autant. (Attention, contradiction repérée, pas grave) Voire pourquoi pas, c'est un salaud qui vous rappellent chaque jour que vous galérez parce que lui peut se permettre de dépenser de l'énergie pour vous aider, alors que vous non. #TireToiUneBalleDansLePieds  

C'est le moment ou ça devient grave. Parfois très grave.

# Et ensuite ?

Ensuite, je connais personnellement 2 manière différentes où ça peut évoluer.

## Cas n°1
Dans le meilleur des cas, le gourou en question fait scission, part créer son projet de son côté avec ses "suiveurs". Imbus d'eux-même, ils sont persuadés d'être dans leur droit, qu'ils feront mieux, qu'il n'y a plus de temps à perdre avec ces gens qui ne peuvent pas les comprendre.  
Souvent ils ne réalisent même pas le soulagement que leur départ apporte. Pour eux, c'est une punition de laisser les pauvres personnes restantes se dépatouiller (true story). On est effectivement dans un mécanisme quasiment sectaire, c'est important de le noter.

En général dans ce cas là, tout dépend de la force de caractère du gourou, mais le groupe finit par s'écrouler. Soit que le gourou s'en désintéresse et laisse tomber ses suiveurs (ne croyez jamais dans un gourou les gens, il s'en branle de votre vie !), les laissant se démerder avec la suite. Soit emportés par leur propre importance, ils ne se dotent pas de la structure suffisante pour survivre et finissent par imploser, très souvent sans même s'en rendre compte, tout contents d'eux qu'ils sont.

C'est déjà triste mais c'est le meilleur des cas, en général le projet d'origine peut s'en relever, si des personnes qui y croient sont encore là. D'expérience, c'est un cas qui arrive plus souvent quand il y a des personnalités fortes dans le projet, des gens qui ont soit les mots, soit le charisme pour menacer suffisamment le gourou et ainsi qu'il préfère éviter le conflit.  
Je connais quelques exemples d'associations qui ont pu se relever d'évènements de ce genre.

## Cas N°2
Dans le pire des cas, le groupe récupère complètement le projet, le transformant en sa propre chose. N'aidant que ceux qu'ils décident d'aider. S'appuyant sur l'existant pour répandre encore plus son esprit de communauté vicié.  
Dans ce cas, la plupart des personnes d'origine auront quittées le projet. Tout ceux qui sont différents auront été mis à la porte. C'est contradictoire avec le fait que le groupe se considère différent et en souffrance ? Rien à foutre ! On a raison ! Et ceux qui ne sont pas d'accord, qu'ils dégagent !

Dans ce cas là, ça se termine souvent comme le premier par une implosion, mais ça peut prendre plus de temps parce que le groupe s'appuie sur une structure et des relations existantes. On mets beaucoup plus de temps à faire pourrir un arbre quand il est en pleine forme, plutôt que quand il commence à peine à pousser.  
En outre, le groupe profite de la légitimité du projet, ce qui rends plus facile de répandre ses idées.

# Comment réagir ?

Alors j'ai déjà bien écris, mais il le fallait pour bien expliquer d'où vient ce phénomène. Parce que la question qui revient le plus souvent quand un projet commence à subir ce phénomène, c'est "Mais pourquoi ?". Pourquoi des gens réagissent comme ça, pourquoi des gens refusent d'écouter, pourquoi prennent-ils des partis pris, pourquoi insultent-ils des gens qui les aide, voire des gens qui sont exactement comme eux ?

Parce qu'il est dans la nature humaine de rechercher un groupe où se sentir en sécurité, et parce qu'il existera toujours des fumiers qui voudront s'en servir. Et je pèse mes mots.

A vous qui le subissez, mais aussi à vous qui avez rejoint une de ces communautés, sachez-le : la source du groupe peut être une religion, une orientation sexuelle, une couleur, peu importe. Ce que recherche le gourou, c'est une étiquette à poser, une faille dans laquelle s'engouffrer pour générer un sentiment d'appartenance.  
Mais surtout, sachez-le : dans 99% des cas, **le gourou n'en a en fait rien à foutre de vous, de ces idéaux, ou de cette étiquette.** Et en général le % restant c'est pareil, mais il est juste persuadé lui-même qu'il est dans son bon droit.

Et donc, sachez-le : vous n'être pas seuls. 

 - Ne vous épuisez pas à tenter d'argumenter : vous aurez toujours tord
 - Ces gens s'en foutent de ce que vous dites, ils n'essaieront même pas de démonter vos arguments. Décrédibiliser est plus facile
 - Ne vous en voulez pas, n'ayez pas l'impression d'avoir merdé. Ce n'est pas le cas, ils exploiteront chaque petite phrase, chaque petite possibilité, pour faire croire que vous êtes un connard
 - Vous n'êtes pas égoïste. Vous avez monté un projet. Le gourou l'est, et il passe son temps à faire croire que vous l'êtes.
 - Vos propos seront repris contre vous, déformés, transformés, voire modifiés, technique classique. Sur les réseaux sociaux, via des screenshot. Si ça arrive, ne tentez pas de vous expliquer, vos propos seront systématiquement déformés de nouveau. Ce n'est pas vous, c'est eux. Mettez vous en retrait, laissez d'autres prendre la bataille.

Techniquement, on est face à ce qui est l'origine du troll, dans toute sa puissance. Une fois cela acquis, vous savez qu'il ne sert à rien de réagir vous-même face à un troll. Tout ce qu'il cherche, tout ce dont il a besoin, c'est de voir que sa victime est touché par son propos.

# Donc, que faire pour l'éviter ?

Malheureusement, je connais assez peu de moyen de se débarrasser complètement de ce genre de mouvement. Il est totalement inhérent à la nature humaine. Et en plus, l'impossibilité d'argumenter et donc d'ouvrir les yeux des fidèles rends très difficile toute tentative de discussion.

Il n'y aura pas ou peu de compromis.

Dans le monde "réel", j'entends par là en dehors des réseaux, en général il devient nécessaire d'exclure du projet le gourou. Il faut le faire avant qu'il ne gagne trop en puissance. Au pire, il part avec plusieurs personnes et on se retrouve dans le cas n°1. Ça reste mieux que le cas n°2.

C'est brutal, c'est violent, et ça demande également une volonté forte de la part des membres du projets, parce que oui, si vous participez à un projet de ce genre, **vous êtes gentils** (ou en tout cas, vous avez de l'empathie pour les autres). Et donc, vous aurez du mal à rejeter quelqu'un simplement parce qu'apparemment il ne pense pas exactement comme vous... Ça fait chier, ça n'est pas facile, ça vous poursuivra. C'est une putain de décision difficile.

C'est également difficile de savoir, de voir venir. On ne peut pas dégager tout le monde parce qu'ils ne pensent pas comme nous, sinon on tombe nous-même dans le communautarisme :-/

Cherchez les mots. "Ils ne méritent pas". "Tu ne peux pas comprendre". "Ne l'aidez pas, il ne fait pas partie de". Le refus du choix de la majorité. Le refus d'argumenter. Les choix manichéens, blanc ou noir.  
Ce sont des indices.

Attention à cibler le meneur, et pas les suiveurs, qui la plupart du temps sont eux aussi victimes, aussi dur est-il de l'admettre. 

Je connais quelques vieux briscards associatifs ou membre de mouvances anarchistes capables de détecter très rapidement ce genre de comportement et d'y mettre direct le hola. Mais c'est un apprentissage, et même une fois détecté, il faut encore avoir les moyens d'agir.

IRL, c'est plus "simple" (toute proportions gardées), il ne suffit pas de changer de pseudo pour revenir. Mais le gourou est rancunier et peut envoyer d'autres personnes à sa place, dénigrer de l'extérieur, etc.


Sur les réseaux, et Mastodon en particulier, c'est plus difficile.

Twitter est moins impacté parce que malgré tout ce que l'on peut dire sur sa modération (et on peut en dire), elle est globalement apartisane (j'insiste sur le "globalement"). Ils sont très proches du free speech américain, où tous les avis ou presque peuvent être énoncés. C'est ce qui fait que des comptes peuvent être bannis pour une simple insulte, alors même qu'ils ont craqués suite à des messages haineux MAIS qui eux sont restés dans le cadre du "free speech".  
Personnellement ça me débecte, mais dans ce contexte une entreprise qui fait du blé n'en a rien à foutre des batailles de clocher, ils appliquent leurs règles et on sait (ou pas d'ailleurs) à quoi s'en tenir, et bisous de leur part.

Le fédiverse est différent. Le fédiverse fait peser sur des admins dont ce n'est pas le métier, et qui ont une opinion, un travail de modération clairement pas simple. Sans parler des instances directement gérées par des gourous.  
C'est très facile de faire peser sur les admins les "fautes" réelles ou imaginaires d'un ou deux utilisateur. 

Oui, parfois il y a de vraies fautes, et de vrais connards. Et ça veut pas dire que les 500 autres utilisateurs le sont aussi. Ni que l'admin qui n'a pas banni le compte dans les 45 secondes est un vendu complaisant. Faites la part des choses !

Je suis vraiment très attaché au système de fédération, et je pense que petit à petit, de manière assez naturelle, il va se créer non pas un mais plusieurs fédiverses. Les communautés se feront leur petit nid, et avec l'impression bienheureuse d'être seule contre le reste du méchant monde, elles se gargariseront de leur importance. Grand bien leur fasse.  
Et malheureusement tant pis pour les pauvres nouveaux qui s'y inscriront par hasard sans la connaître, la bouche en coeur, et finiront en général par fuir en courant s'ils ne se sont pas fait bannir avant uniquement parce qu'ils ne collent pas à l'étiquette. On ne peut pas y faire grand chose, la communication a ses limites, il est nécessaire de l'accepter. 
 
On le constate déjà d'ailleurs avec les instances regroupant les fameux 12-25, ou probablement Gab qui va arriver. Ils sont déjà dans leur monde. Ils y restent.

C'est une foutue guerre permanente, parce qu'il y aura **toujours** des communautés de ce genre qui se formeront, parce qu'il sera toujours plus facile de passer d'opprimés à oppresseurs que de vivre en bonne intelligence. Ça peut se comprendre, après tout on en a chié, on nous donne un pouvoir, autant en profiter pour se venger non ? Vous voyez la vicieuseté du truc...  
Parce qu'il est toujours plus facile d'attaquer ceux qui nous sont proches et qui nous aident, plutôt que ceux qui sont les vrais méchants et nous oppriment. Parce que les méchants sont forts, ils sont équipés, ils ne lâchent rien.

Et donc, que faire ?!?

Vous êtes la cible d'un harcèlement d'une communauté de ce genre, personnellement, mon seul conseil et il est pas facile ni à donner ni à appliquer c'est : mettez vous en retrait.  
De toute façon, quoi que vous disiez, ça sera déformé et vous passerez pour un connard. Ne répondez pas aux citations, pour peu que vous soyez énervés (et vous avez carrément le droit de l'être), ça sera récupéré et déformé pour justifier les attaques contre vous.

Posez les choses, créez un nouveau thread, expliquez vous de manière générale, mais pas pour ces gens. Pour tous les autres gens.  
Si vous avez la "chance" d'être une personnalité à peu près en vue (il y a de bonne chance, ce sont les meilleurs cibles), d'autres pourront vous défendre, expliquer qui vous êtes, mettre en face les contradictions. Bon, je pondère cette notion de "chance d'être une personnalité" vu que c'est probablement une des causes pour que vous soyez ciblé, attention. 

Il n'y a rien de plus doux pour un gourou qu'une cible démunie qui perds son sang froid. Il n'y a rien de pire pour lui qu'un groupe se liguant pour montrer à ses fidèles qu'il se fourre le doigt dans l'oeil jusqu'au coude. Il n'y a rien à lui prouver à lui, il le sait parfaitement.

Chacun encaisse comme il le peut. L'un de mes conseils seraient par exemple de carrément fermer Mastodon le temps que ça se décante, mais ça peut être très difficile. On ne peut pas forcément se déconnecter, ça donne une impression de fuite, d'abandon, mais ça n'est pas ça.  
Vous-n'êtes-pas-seuls. Ne vous laissez pas bouffer. Putain c'est dur, moi-même ça m'est arrivé de péter un plomb, et pourtant ceux qui me connaissent pourront vous dire que si je me suis réellement énervé 5 fois dans ma vie c'est le bout du monde.  
Retirez-vous quelques temps, laissez d'autres mener cette bataille là, et revenez regonflé à bloc.


Vous observez un harcèlement de ce genre, vous connaissez/appréciez le harcelé, n'hésitez pas à lui offrir du soutien. Un favori sur un de ses messages. Un petit message disant juste "Soutien, je suis avec toi." Il ne vous répondra pas, il n'en a peut être pas le temps, pas l'envie, pas l'énergie, ne vous en formalisez pas.  
Chaque petit soutien fait comme un baume au coeur, un petit sursaut de "des gens m'aiment !" au milieu des vagues de haine et de rejet. C'est important.


Vous voyez passer un message qui vous hérisse, vous ne le comprenez pas, vous le trouvez insultant. Si vous souhaitez y réagir, prenez la peine de regarder le contexte. Cherchez d'où il vient, quel en est l'origine. N'oubliez **jamais** qu'il peut venir d'une personne sans aucune mauvaise intention qui a fait une mauvaise blague (et oui, ça nous arrive à tous), qui n'a pas votre vécu (ou pire, qui l'a et qui a justement réussi à prendre du recul dessus).  
Si vous ne souhaitez pas prendre cette peine d'étudier le contexte avant de réagir, alors **ne réagissez pas**. Vous n'avez pas la moindre idée des dégâts que vous pourrez peut être faire en réagissant à chaud à un message qui a potentiellement été complètement déformé. Peut être que vous serez LE message de trop après les 2000 premiers qui viendront de tomber sur la tronche d'une personne qui n'a rien demandé et papotait juste avec un pote de la soirée d'hier.

Vous êtes un harceleur et vous êtes arrivés jusqu'ici, bravo, franchement ça me fait plaisir :-)  
Réfléchissez à pourquoi vous réagissez comme cela, à pourquoi vous attaquez des personnes qui à priori vont dans votre sens. Est ce qu'il n'y aurait pas des cibles plus cohérentes pour votre énergie ? Qui vous pousse à réagir de cette façon ? D'où vient l'énervement que vous ressentez au fond de vous dès que vous voyez passer une notification de un(e)tel(le) ?  
Est ce que quelqu'un ne vous aurait pas orienté dans la bonne direction avant de vous lâcher, tel le chien qu'on balance et qu'on oublie ?  
Ré-flé-chi-ssez !


Ne vous laissez pas transformer. Il arrivera peut-être que l'un de ceux qui vous a pourri aura un jour besoin d'aide.  
Il peut même arriver que vous avez déjà aidé l'un de ceux qui vous a pourri. Et qu'il aura de nouveau besoin d'aide.  
N'oubliez pas, les gourous sont les plus à blâmer. Ça aussi c'est dur, mais si vous arrivez à comprendre et à aider leurs victimes, alors il aura perdu.  

Certains sont irrécupérables. Ils ont trop besoin d'être guidé, d'avoir un objectif, qu'on leur dise comment réagir à leur place. Ils ont été élevés comme ça, ou ils en ressentent le besoin.  
Même eux, aidez-les.


Mais surtout, surtout, faites attention à vous, prenez soin de vous. Vous n'êtes pas seuls. <3 
