---
Title: Ceph - utiliser le reste d'un disque partitionné
date: 2019-02-26 06:53
Authors: Victor
Slug: ceph-utiliser-disque-restant
Category: Système
Tags: ceph, astuces
keywords: ceph, astuces
Status: published
---

[TOC]

# Introduction et disclaimer
Parfois sur un serveur, on se retrouve avec un disque de grande capacité qui ne vous sert à rien, parce que par exemple vous avez juste besoin de quelques centaines de Go pour installer un système.

Si vous utilisez en outre un système de stockage distribué (ici [ceph](https://ceph.com)), cela rend cette perte encore plus dommage, puisque la tentation est grande d'utiliser le reste du disque pour l'intégrer au cluster.

Avec ceph, c'est possible !  
En effet comme il se sert de partitions toute simple et non d'un disque entier, il est possible de le nourrir avec ce qu'il reste d'espace sur votre disque.

Il y a bien sûr des contraintes que l'on va détailler dans les disclaimers suivants.

## Disclaimer 1
Mieux vaut que la taille restante de votre disque soit proche de celle des autres disques du cluster.

**Ajouter 100Go de disque à un cluster utilisant des disques de 3To n'a au mieux aucun intérêt et au pire peut être risqué !**

## Disclaimer 2
**GROS DISCLAIMER**

Utiliser le disque où il y a votre système installé va partager les IO du disque entre ceph et l'OS ! 

Ceph peut être un très gros consommateur de lecture/écriture, il est donc **important** de ne faire ça que si votre système a besoin de peu d'io.

 - Ne le faites pas si vous utilisez un disque lent (5400trs/min ? c'est mort. 7200 trs/min ? Ça peut passer si votre OS n'écrit rien)
 - Ne le faites pas si votre OS utilise de nombreux IO (service de BDD installé dessus, nombreux logs, ...)
 - Surveillez vos IO et la charge dans le temps après la configuration
 - Ne le faites pas si vous ne savez pas ce que vous faites :kissing:hearth:
 
# Manipulations
 
L'idée est simple. On va formater le disque à la main et s'en servir pour l'intégrer dans ceph.

Comme toute idée simple, le potentiel de ratage est important, je vais donc volontairement détailler les choses auxquelles faire attention.

Ouais je sais je suis un peu chiant, que voulez vous, c'est l'apprentissage de la vie en production qui parle :-D

## Hypothèses 
**À lire**

Je pars ici du principe que les binaires ceph sont déjà installés sur votre système (si par exemple vous utilisez un autre disque du serveur pour ceph)  
Si ce n'est pas le cas, commencez par attaquer la [documentation](http://docs.ceph.com/docs/master/install/) :-)

Je vais également prendre un **exemple** de partitionnement existant pour la numérotation des partitions.  
**Adaptez** cet exemple avec votre situation pour éviter d'écraser des partitions existantes !

## Notre exemple

Dans cet exemple on va se baser sur un disque de 3To qui possède déjà 4 partitions :

```bash
# fdisk -l /dev/sda
Disk /dev/sda: 2.7 TiB, 3000592982016 bytes, 5860533168 sectors

Device         Start        End    Sectors  Size Type
/dev/sda1       4096   67112959   67108864   32G Linux swap
/dev/sda2   67112960   68161535    1048576  512M Linux filesystem
/dev/sda3   68161536  277876735  209715200  100G Linux filesystem
/dev/sda4       2048       4095       2048    1M BIOS boot
```

Comme vous le voyez on utilise déjà environ 132Go, on va donc s'occuper de récupérer les 2,8To restant.

**Là encore, pensez à adapter les numéros de partitions dans la suite !**

On va également utiliser **sgdisk** car c'est l'outil de configuration qu'utilise ceph lui-même quand il doit préparer un disque complet, et il fait le taf exactement comme les binaires ceph s'attendent à le voir fait.

## Partitionnement

Ceph a besoin d'une partition de type `Ceph OSD` d'une centaine de Mo pour ses méta données, qui **doit** porter le nom `ceph data`.  
On va donc lui créer une 5eme partition nommée `ceph data` de type `4fbd7e29-9d25-41b8-afd0-062c0ceff05d`. (cf [wikipedia](https://en.wikipedia.org/wiki/GUID_Partition_Table))

**Ici modifiez les `5:` pour coller avec votre propre numéro de partition.** Il y a quatre `5:` à changer.
```bash
sgdisk --new=5:0:+100M --change-name="5:ceph data"   --partition-guid=5:$(uuidgen -r)   --typecode=5:4fbd7e29-9d25-41b8-afd0-062c0ceff05d -- /dev/sda
partprobe # partprobe nous permet ici de relire à chaud les partitions d'un disque en cours d'utilisation.
```

On va ensuite créer une 6ème partition utilisant tout le reste du disque.  
Celle-ci doit s'appeler `ceph block` et être de type `CAFECAFE-9B03-4F30-B4C6-B4B80CEFF106` (oui oui cafe cafe, là encore confer [wikipedia](https://en.wikipedia.org/wiki/GUID_Partition_Table))

**Là encore** modifiez les numéros `6:` pour coller à votre partition. Il y a trois `6:` à changer cette fois.
```bash
sgdisk --largest-new=6 --change-name="6:ceph block"   --typecode=6:CAFECAFE-9B03-4F30-B4C6-B4B80CEFF106 -- /dev/sda
partprobe
```

## Activation

Une fois le partitionnement terminé, il ne nous reste plus qu'à dire à ceph d'utiliser le disque :

**Comme toujours**, modifiez vos partitions pour coller à votre cas ! Ici `sda5` représente la partition ceph-osd, `sda6` le reste du disque.
```bash
ceph-disk prepare --bluestore /dev/sda5 /dev/sda5 /dev/sda6
ceph-disk activate /dev/sda5 
```

Et voila, une fois le `ceph-disk activate` terminé, votre nouvel osd est prêt et intégré au cluster ceph !

Enjoy :-)