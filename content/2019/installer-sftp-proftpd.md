---
Title: Installer un serveur SFTP avec ProFTPd
date: 2019-03-26 06:48
Authors: Victor
Slug: installer-sftp-proftpd
Category: Système
Tags: sftp, astuces, proftpd
keywords: sftp, astuces, proftpd
Status: published
---

[TOC]

# Pourquoi sftp ?

Le protocole [FTP](https://fr.wikipedia.org/wiki/File_Transfer_Protocol) est très utilisé dans le monde de l'hébergement
 web (mais pas que) pour transférer des fichiers grâce à sa robustesse et sa simplicité.
 
Cependant, il n'est pas sécurisé par défaut, et tous les transferts se font donc en clair, ce qui à mon sens est l'un de ses plus gros défaut.  
Comme pour pas mal d'autre protocoles, il existe FTPs qui rajoute une couche SSL par dessus FTP, mais de la même manière que les autres protocoles, 
elle reste relativement complexe à configurer, nécessite un certificat SSL à renouveler, etc.

C'est pourquoi nous allons parler ici de [sftp](https://fr.wikipedia.org/wiki/SSH_File_Transfer_Protocol) qui se sert de la couche SSH déjà présente sur 
la plupart des serveurs Linux pour sécuriser FTP.

Plusieurs avantages à la fois par rapport à FTPs et par rapport à scp (qui est le système de transfert intégré à ssh)

 - On utilise des clefs ssh pour le chiffrement, pas de renouvellement, pas de signature par un tiers
 - On peut utiliser toutes les commandes de manipulations de fichier de FTP, plus avancées que scp
 - La configuration d'un chroot est ultra simple car elle peut s'appuyer sur les mécanismes intégrés de FTP
 - On peut utiliser une clef SSH pour l'authentification
 - On élimine le problème de connexion passif/actif de FTP
 - La plupart des logiciels FTP du marché supportent également sftp

 
Dans cet article qui se veut un pense-bête, j'utiliserai (proftpd)[http://proftpd.org/] pour implémenter sftp car la configuration est plutôt intuitive et simple, facile à automatiser.


# Installation de proftpd

## Installation

**Note** Ce tutoriel se base sur Debian car je n'ai pas de centos sous la main. Néanmoins, mis à part le gestionnaire de paquet et peut être l'emplacement des fichiers de conf, la configuration est similaire. 

Proftpd étant supporté par la plupart des distributions connues, l'installation se fait via le gestionnaire de paquets :

```bash
sudo apt-get install proftpd
```

Une fois installé, éditez le fichier `/etc/proftpd/proftpd.conf` et modifiez le `ServerName`. Cela représentera le nom du serveur présenté lors de la connexion.

```bash
ServerName "ftp.monserveur.tld"
```

## chroot

L'un des mécanismes les plus plaisant de proftpd est sa capacité à permettre de manière très simple l'utilisation d'un [chroot](https://fr.wikipedia.org/wiki/Chroot).

Concrètement, l'utilisateur sera "bloqué" dans son répertoire parent sans aucun moyen d'en sortir, et notamment sans moyen d'aller se balader sur le reste du système.  
Il ne pourra voir que le répertoire à l'intérieur duquel vous l'avez restreint, et ses sous répertoires.

Pour cela, éditez le fichier `/etc/proftpd/proftpd.conf`. Trouvez la directive `DefaultRoot`, qui est par défaut commentée (par un `#`).

Décommentez la directive :

```bash
DefaultRoot ~
```

Et c'est tout !

Bien entendu n'oubliez pas de redémarrer proftpd après cette modification.

```bash
systemctl restart proftpd.service
```

# Activation de sftp

Une fois ces préambules terminés, on va attaquer sftp lui-même.

Pour cela on va utiliser une technique très similaire aux "virtual hosts" d'un serveur web.

Créez le fichier `etc/proftpd/conf.d/sftp.conf` et éditez le. On va le remplir avec un hôte virtuel proftpd :

```bash
<IfModule mod_sftp.c>

        SFTPEngine on
        Port 2222
        SFTPLog /var/log/proftpd/sftp.log

        # On utilise les même clefs ssh que celles du serveur openssh
        SFTPHostKey /etc/ssh/ssh_host_rsa_key
        SFTPHostKey /etc/ssh/ssh_host_dsa_key

        SFTPAuthMethods publickey password

        SFTPAuthorizedUserKeys file:/etc/proftpd/authorized_keys/%u

        RequireValidShell off
        AuthOrder mod_auth_file.c

        # Enable compression
        SFTPCompression delayed

</IfModule>
```

## Signification de la configuration

La configuration que l'on indique pour sftp se construit de la manière suivante :

 - `IfModule` n'active ce bloc que si proftpd contient le module sftp (ce qui est le cas dans les paquets en général)
 - `SFTPEngine on` va activer sftp
 - `Port` vous permet de spécifier le port d'écoute. Choisissez un port différent du FTP standard (port 21)
 - `SFTPLog` vous permet de spécifier un fichier de log spécial pour les connexions sftp
 - `SFTPHostKey` va définir quelles clefs SSH utiliser pour chiffrer la connexion
 - `SFTPAuthMethods` permet de spécifier quelles méthodes sont authorisées pour la connexion à sftp
    - `publickey` va vous permettre d'utiliser un fichier de clef ssh publiques
    - `password` va vous permettre d'utiliser le système utilisateur/mot de passe classique de votre serveur ftp
 - `SFTPAuthorizedUserKeys` est le fichier qui contient les clefs publiques pour autoriser la connexion
 - `RequireValidShell` va désactiver le fait que vos utilisateurs doivent disposer d'un shell valide (utile pour les utilisateurs virtuels)
 - `AuthOrder` définit quel type d'authentification utiliser en priorité. Ici un fichier pour nos utilisateurs virtuels
 - `SFTPCompression` définit de quelle manière compresser le flux pendant la connexion. `delayed` signifie que sftp n'activera la compression que si l'authentification est réussie
 
# Utilisateurs virtuels
 
Un autre des mécanismes sympas de proftpd, c'est sa capacité à gérer les utilisateurs virtuels.

Avec la conf ci-dessus, vous pouvez vous connecter avec des utilisateurs systèmes, des utilisateurs qui existent sur votre serveur.

Outre le fait que c'est gênant niveau sécurité (il ont un UID/GID existant, ils peuvent tenter d'accéder à un shell, ...), c'est peu pratique si vous avez beaucoup d'utilisateurs.  
Pour compenser ce défaut, on va utiliser des utilisateurs virtuels, c'est à dire qu'ils n'existent que du point de vue de proftpd.

Ici j'utiliserai une configuration assez simple avec des utilisateurs dans des fichiers, mais sachez qu'il est possible de brancher proftpd à une base de données pour récupérer les utilisateurs.

## De la considération sur les uid

Pour des utilisateurs virtuels, rien n'oblige à utiliser un `uid` correspondant à un véritable utilisateur du système, puisqu'il n'existe que dans proftpd.

Néanmoins, il est **important** de prendre en compte que cet utilisateur va créer des fichiers sur le système, qu'ils auront l'uid de l'utilisateur, et qu'il va donc y avoir intéraction entre un espace virtuel et le système tout ce qu'il y a de plus physique.

Quelques considérations sont donc à garder en tête
 
 - N'utilisez pas l'uid `0` qui correspond à root
 - Utilisez un uid au dessus de 1000, ceux en dessous étant réservés aux services du système
 - Si vous devez intéragir avec un service (serveur web par exemple), vous pouvez utiliser son uid pour que l'utilisateur ait le droit de toucher aux fichiers.
 - **N'utilisez pas l'uid `0` qui correspond à root !**
 - Évitez d'utiliser un uid correspondant à un utilisateur existant pour éviter les fuites
 - N'oubliez pas que le répertoire de l'utilisateur virtuel devra coller à l'uid virtuel ou il ne pourra rien écrire
 - N'utilisez-pas-l'uid `0` qui-correspond-à-root. Merci. 😘
 - Il en va de même pour le GID (groupe id) que pour l'uid

## Activer les utilisateurs virtuels sur proftpd

Pour permettre d'utiliser des fichiers contenant nos utilisateurs virtuels, on va créer le fichier `/etc/proftpd/conf.d/auth.conf`.

À l'intérieur indiquez :

```bash
AuthGroupFile /etc/proftpd/ftp.group
AuthUserFile /etc/proftpd/ftp.passwd
```

Ensuite vous pouvez créer vos utilisateurs avec la commande suivante :

```bash
ftpasswd --passwd --file=/etc/proftpd/ftp.passwd --uid=UID --home=/path/to/home --shell=/bin/false --sha256 --name=USERNAME
```
Un prompt vous demandera de taper le mot de passe de votre utilisateur.

 - `--passwd` indique que l'on créé un utilisateur à stocker dans un fichier AuthPasswd
 - `--file` spécifie le fichier où stocker l'utilisateur
 - `--uid` l'uid de votre utilisateur, selon votre choix précédent
 - `--home` le répertoire où votre utilisateur sera restreint
 - `--shell` par sécurité, on va désactiver le shell pour l'utilisateur
 - `--sha256` le mot de passe doit être chiffré en sha256
 - `--name` est bien sûr le nom de votre utilisateur

**Note :** ici on créé un groupe par défaut qui a le même gid que l'uid utilisateur.  
Pour affiner les droits, on peut ensuite créer un groupe spécifique :

```bash
ftpasswd --group --file=/etc/proftpd/ftp.group --gid=GID --member=USERNAME1 --member=USERNAME2 --name=GROUPE1
```

 - `--group` indique que l'on va créer un groupe à stocker dans un fichier AuthGroup
 - `--file` est le fichier où stocker ce groupe
 - `--gid` le groupe id à utiliser pour votre groupe
 - `--member` la liste des utilisateurs à inclure dans le groupe. Plusieurs occurences permettent d'ajouter plusieurs utilisateurs
 - `--name` le nom de votre groupe
 

Après ça, vous pouvez utiliser votre serveur ftp pour vous connecter en sftp de manière sécurisé !