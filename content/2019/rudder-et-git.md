---
Title: Rudder et Git
Subtitle: - association pleine de promesses
date: 2019-12-08 20:19
Authors: Victor
Slug: rudder-et-git
Category: Système
Tags: rudder, astuces, git
keywords: rudder, astuces, git
Status: published
---

[TOC]

# Git ?

Dernièrement, on est tombé sur un truc par hasard en bossant avec Rudder.

Tout le système de directives est en fait stocké et versionné grâce à [git](https://fr.wikipedia.org/wiki/Git), c'est ce qui permet de gérer les retours en arrière et également de spécifier des messages dans l'interface pour expliquer les changements.

Il en dérive du coup des possibilités assez intéressantes, comme le fait de pouvoir stocker ces infos sur un git distant, utiliser un serveur de test pour vérifier les techniques, fonctionner avec des Merge Requests, etc.

Je vais ici proposer une configuration basique, mais il est possible ensuite d'utiliser toute la puissance de git avec rudder ^^

# Configuration

Il vous faut bien sûr un serveur rudder fonctionnel, ainsi qu'un repository git dans lequel vous stockerez vos directives.

Étant donné tout ce qui est stockés dans les directives (mots de passe, utilisateurs, clefs ssh, ...), je vous conseille de garder ce repository privé :-)

Rendez vous sur votre serveur rudder, et connectez vous avec votre utilisateur rudder (en général, root)

```
$ cd /var/rudder/configuration-repository
```

Vous allez avoir besoin d'une clef ssh pour vous connecter automatiquement

```
$ ssh-keygen -t rsa -b 4096 -C "votre.rudder.tld"
### N'indiquez pas de mot de passe pour permettre la connexion automatique ###
$ cat ~/.ssh/id_rsa.pub
```

Créez un utilisateur sur votre repo git et configurez-le avec la clef ssh récupérée plus haut pour que votre utilisateur puisse se connecter.

Vous pouvez vérifier la connexion via :

```
$ ssh -T gituser@votre.git.tld
```

On va ensuite ajouter le nouveau repo git et l'initialiser :

```
$ cd /var/rudder/configuration-repository
$ git remote add origin https://url_de_votre_repo_git
$ git status
$ git push --set-upstream origin master
```

Enfin, pour automatiser le push lors que vous modifiez des choses dans l'interface web de rudder, on va créer un hook git :

```
$ cd /var/rudder/configuration-repository
$ cat <<EOF > .git/hooks/post-commit
#!/bin/bash
git push origin master
EOF
$ chmod +x .git/hooks/post-commit
```

Le hook est simple et va juste envoyer un `git push` vers votre repo distant dès qu'un commit est effectué via l'interface web, ce qui permettra de garder le repo à jour.

# Aller plus loin

Avec ce système, vous pouvez envisager d'industrialiser l'utilisation de rudder !

En plus des possibilités de sauvegardes/restauration très faciles des directives en cas de boulettes, et de suivi précis de ce qui est fait et par qui, quelques autres effets kiss cool sont possibles.

En effet, l'un des "inconvénient" de rudder est qu'il permet trèèès facilement de tout casser sur vos serveurs en déployant une directive foireuse.
Maintenant, il devient très simple de créer un serveur rudder de test, utilisant par exemple des machines virtuelles, et publiant les modifications sur une branche de développement.

Et une fois les directives testées, une merge request, une vérification des collègues, un petit merge dans la branche master, et hop ! On envoie en production les nouvelles directives ! 😘

En gros, intégrer rudder à un système de déploiement continu et de tests est largement facilité :-)

N'hésitez pas à proposer vos utilisations et astuces utilisant rudder et git dans les commentaires !
