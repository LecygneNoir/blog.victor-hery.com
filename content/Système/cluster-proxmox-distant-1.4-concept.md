Title: Le concept du cluster
subtitle: ; cluster Proxmox distant (1/4)
Date: 2013-01-16 19:00
Modified: 2013-01-16 19:00
Category: Système
Tags: proxmox, système, haute disponibilité, virtualisation, cluster
keywords: proxmox, système, haute disponibilité, virtualisation, cluster
Slug: cluster-proxmox-distant-1.4-concept
Authors: Victor
Status: published

[TOC]

## Commençons par le début

<p>Oui, car ça fait un petit moment que je souhaite écrire une série d'articles sur Proxmox, articles présentant la mise en place de A à Z d'un <a href="http://fr.wikipedia.org/wiki/Grappe_de_serveurs">cluster</a>.</p>

<p>En effet, Proxmox depuis sa version 2 propose un système de clustering basé sur Red Hat, qui présente un potentiel très intéressant de fonctionnalités pour parvenir à un système très stable.</p>

<p>Accessoirement, ça présente également un potentiel de problèmes clairement non négligeables. Mais bon.</p>

<p>Dans cette série d'articles, je vais donc tâcher de présenter sous le biais de tutoriel, la mise en place d'un tel cluster. Cependant, comme il est difficile de couvrir tous les cas possibles, je vais me permettre de me placer dans un cas précis et défini, adaptable et qui correspond à une problématique assez difficile.</p>

<p><strong>L'idée va être de configurer 3 serveurs distants géographiquement en cluster proxmox, avec un système de haute disponibilité.</strong></p>

<p>Dans ce premier article, je vais donc présenter la problématique du système, ainsi que le vocabulaire et les concepts importants à comprendre et à retenir. Les prochains articles seront plus techniques, promis !</p>

<p>Néanmoins, je partirais du principe que vous avez déjà une petite expérience de Proxmox (au moins savoir l'installer !), et que vous maitrisez SSH tout en ayant des bases de réseaux. J'essaierai quand même de rendre le plus compréhensible possible les problématiques techniques !</p>

<p>Je m'excuse d'avance pour la longueur des articles, mais la réalisation est quand même assez complexe&nbsp;;-)</p>

<p>Pour ces articles, je m'appuie principalement sur les 3 liens suivants (merci beaucoup à eux pour leur travail !)</p>

<ul>
	<li>Blog du développeur neurasthénique; <a href="http://blog.developpeur-neurasthenique.fr/auto-hebergement-configurer-un-cluster-proxmox-2-sans-multicast.html">cluster proxmox via OpenVPN</a></li>
	<li>NedProduction :&nbsp;<a href="http://www.nedproductions.biz/wiki/configuring-a-proxmox-ve-2.x-cluster-running-over-an-openvpn-intranet">Configuring a Proxmox VE 2.x cluster running over an OpenVPN intranet</a> (notamment la partie 3)</li>
	<li><a href="https://alteeve.ca/w/2-Node_Red_Hat_KVM_Cluster_Tutorial">Red Hat cluster tutorial</a> (extrêmement complet si vous lisez l'anglais. Une mine !)</li>
</ul>

<p>Je me suis également servi du <a href="http://forum.proxmox.com/forum.php">forum</a> et du <a href="http://pve.proxmox.com/wiki/Main_Page">wiki</a> proxmox.</p>

## Vocabulaire

<ul>
	<li><a href="http://fr.wikipedia.org/wiki/Grappe_de_serveurs">Cluster</a> : comme le dit wikipédia, un cluster est un ensemble de serveurs regroupés artificiellement ensemble pour permettre la gestion globale de ceux-ci, et éventuellement la mise en place d'un système de haute disponibilité.</li>
	<li>Node : une node est le nom donné à un serveur à l'intérieur d'un cluster. Un cluster est constitué de nodes, indépendantes ou non les unes des autres.</li>
	<li><a href="http://fr.wikipedia.org/wiki/Haute_disponibilité">Haute disponibilité</a> : la haute disponibilité (High Availability, ou HA en anglais) consiste à assurer un service continu à vos utilisateurs/clients, même en cas de panne matérielle. Cela devient une vraie problématique dans les systèmes informatiques actuels, ou la simple indisponibilité pendant quelques heures d'un site web (le temps de changer un disque dur défectueux par exemple), peut rapidement se chiffrer en pertes financières importantes. La haute disponibilité permet donc de détecter automatiquement une panne et d'y réagir rapidement pour proposer une continuité de service. Cette continuité de service peut éventuellement être dépréciée, selon les standards recherchés. Par exemple le site web accessible, mais la plate-forme de paiement en ligne coupée temporairement.</li>
	<li><a href="http://fr.wikipedia.org/wiki/Plan_de_reprise_d'activité">Plan de Reprise d'Activité</a> : Le Plan de Reprise d'Activité (PRA) est la procédure mise en place justement pour permettre une continuité de service. Cette procédure doit donc permettre de remettre en route tout ou partie des services en cas de panne du service principal, dans un délai prévu d'avance. Le PRA doit faire l'objet de tests réguliers et être bien assimilé par tout le monde, notamment dans le cas où les services remontés en cas de panne sont dépréciés. Le PRA peut également être le nom donné affectueusement au(x) serveur(s) qui servent à remonter les services&nbsp;:-)</li>
	<li><br />
	Sauvegardes : les sauvegardes consistent à garder une trace régulièrement des machines virtuelles ou des dossiers importants sur un serveur. Elles font en général partie intégrante d'un PRA, et elles doivent être gardées dans différents endroits physiques pour pallier à tout risque de perte en cas d'incendie ou autre problème grave.&nbsp;<strong>Attention, mettre en place un système de HA ne doit pas dispenser de faire des sauvegardes !</strong></li>
	<li><a href="http://fr.wikipedia.org/wiki/Quorum">Quorum</a> : le quorum est un concept assez difficile à appréhender, et pourtant il est absolument nécessaire à comprendre pour travailler sur un cluster en HA. Je vais donc m'arrêter un peu dessus pour expliquer le principe. D'après la page wikipédia, un quorum est le nombre minimal de personnes nécessaires pour prendre une décision dans un groupe. C'est un terme utilisé en droit habituellement, et un quorum représente en général la majorité, si tout le monde vote pour une voix. En informatique, le quorum est le nombre minimal de <em>votes</em> à atteindre pour prendre une décision <em>automatiquement</em>, comprendre sans intervention humaine.</li>
</ul>

<p style="margin-left: 40px;">Dans le cas d'un cluster, on peut donner à chaque serveur du cluster un poids différent ou identique, qui va influencer sur les choix que va prendre l'intelligence du cluster en cas de besoin. Par exemple, tous les serveurs ont un poids de un. S'il y a 5 serveurs, le quorum va être de 3. Il faut être au moins 3 pour prendre une décision. Dans ce cas, en cas de panne par exemple sur la liaison réseau entre les serveurs, ceux-ci prendront des décisions en fonction de leur quorum. Prenons un exemple :</p>

<p style="margin-left: 40px;">Nous avons 5 serveurs, montés en HA, avec un poids de 1 chacun. Une panne réseau survient, qui isole 3 serveurs d'un côté et 2 de l'autre. Chaque serveur va vérifier la connectivité avec les autres. Les serveurs ayant le quorum (le groupe de 3), se trouvant en majorité, peuvent prendre la décision de démarrer chez eux les services précédemment hébergés chez les 2 serveurs manquants. Les serveurs n'ayant plus le quorum (le groupe de 2) vont par contre décider qu'ils ne sont plus en majorité, et donc déduire que d'autres ayant le quorum vont prendre leur place. Ils vont donc prendre la décision de couper leurs services, voire de s'éteindre en attendant une intervention extérieure.</p>

<p style="margin-left: 40px;">Ce système permet d'éviter que des services tournent en même temps à deux endroits différents. Imaginez par exemple que la connexion entre les 2 groupes saute, mais que la connectivité Internet de chaque groupe fonctionne encore. Sans quorum, alors les utilisateurs seraient dirigés sur le site web de gauche ou de droite, rendant ingérable la restauration. En effet, si le site est un forum, alors les utilisateurs ont postés des messages à droite ET à gauche, il faudrait donc les fusionner pour revenir à un état stable sans perdre de données ! C'est un casse-tête qui a été réglé par la notion de Quorum.</p>

<p style="margin-left: 40px;">Bien sûr, ce système n'est pas parfait. Pour reprendre notre exemple, si le problème de connectivité fait que nous avons deux groupes de 2 serveurs et un groupe de 1, aucun des groupes n'a le quorum et tous risquent de s'éteindre. De la même façon, si nous avons un nombre pair de serveurs (Par exemple 4), le quorum est exactement la moitié, ce qui fait qu'en cas de répartition égale des groupes de serveurs, chacun va décider de démarrer les services de l'autre groupe !</p>

<p style="margin-left: 40px;">En conséquence, il est très important de bien comprendre cette notion. Dans l'idéal, vous devez <strong>toujours</strong> monter des clusters avec un nombre impair de serveurs, quitte à ce que l'un deux soit juste une machine virtuelle dans un coin servant 'd'arbitre'. Monter un cluster pair est possible, mais vous acceptez alors le risque de créer des situations qui seront difficilement gérables.</p>

<ul>
	<li><a href="http://en.wikipedia.org/wiki/Fencing_(computing)">Fencing</a> : le fencing n'a pas vraiment d'équivalent en français. C'est une notion liée à la haute disponibilité de la même façon que le quorum, et qui sert principalement à éviter des situations où les services tournent sur deux parties du cluster indépendantes. Le fencing consiste à pouvoir isoler chaque node du cluster pour être sûr qu'elle ne reviendra pas inopinément.</li>
</ul>

<p style="margin-left: 40px;">Par exemple, si un serveur redémarre par accident, le cluster se voyant en majorité va démarrer les services de la node défaillante. Sauf que une fois la node redémarrée, elle va rentrer de nouveau dans le cluster, donc avoir la majorité, et donc décider de lancer ses services. Ce qui va provoquer de nouveau une situation de doublon.</p>

<p style="margin-left: 40px;">Le rôle du fencing est donc d'autoriser le groupe du cluster ayant le quorum (ayant la majorité) à isoler artificiellement la ou les nodes défaillantes. Cela peut se faire en coupant le courant de la node (via un onduleur pilotable en réseau, via une carte DRAC ou ILO, ...), en coupant son réseau (via un switch manageable), en lui envoyant en boucle des ordres d'exctinction en ssh, ... Plusieurs techniques sont disponibles. Ce concept est important, et en général le plus compliqué et le plus lourd à mettre en place techniquement. Accepter de s'en passer peut poser un certains nombre de problème à l'administrateur, nous en parlerons plus loin. Dans Proxmox, le système de HA ne fonctionne pas si le fencing n'est pas configuré correctement.</p>

<p>Globalement, le quorum sert à pallier à une panne <strong>sur la liaison</strong> entre les machines, alors que le fencing sert à pallier à une panne <strong>matérielle</strong> de la machine. Ces deux concepts fonctionnent donc côte à côte.</p>

<ul>
	<li><a href="http://fr.wikipedia.org/wiki/Fail_over">Fail-over</a> : le principe du Fail-Over s'applique en majorité aux adresses IP publiques d'un serveur (parfois à son adresse MAC). Le principe d'IP fail-over est que dans le cas où un de vos serveurs tombe en panne, votre hébergeur est capable de router votre IP vers un nouveau serveur. Techniquement, chacun des serveurs est une copie de l'autre, et chacun des serveurs porte l'IP publique. Cependant, un seul des serveurs est joignable à un instant T, ce qui permet de manière bien pratique de ne pas avoir à changer tous les enregistrements DNS en cas de panne du serveur principal (les DNS pouvant mettre jusqu'à 48h à se rafraichir, on imagine bien qu'une telle technique est utile pour éviter que son site soit innacessible pendant 2 jours).</li>
</ul>

<p>Voila pour le vocabulaire. Si d'autres mots vous posent problèmes durant la lecture d'un des articles, indiquez le dans les commentaires, je le rajouterai ici.</p>

<p>&nbsp;</p>

## Problématique

<p>Dans le cadre de la mise en place d'un <a href="http://fr.wikipedia.org/wiki/Grappe_de_serveurs">cluster</a>, le principe basique consiste à mettre plusieurs serveurs identiques dans une baie, et à se servir d'eux pour augmenter la disponibilité de ceux-ci.</p>

<p>Par exemple, vous avez 3 serveurs, si l'un d'eux tombe, vous basculez tous ses services quasiment instantanément sur les deux autres, vous permettant d'avoir une disponibilité des services continue, même en cas de panne matérielle.</p>

<p>De plus le cluster vous permet de gérer, depuis une seule interface, tous vos serveurs.</p>

<p>C'est ce que propose Proxmox V2. En montant différents serveurs en cluster, vous pouvez en vous connectant à l'interface web de n'importe quel serveur créer des machines virtuelles sur le serveur de votre choix, gérer les sauvegardes de manière globale, créer des droits d'accès, bref, faire tout ce qui est possible d'habitude, mais sur tous vos serveurs d'un coup !</p>

<p>En fait, la vraie problématique qui se pose est la suivante :</p>

<p><strong>Quid si vous voulez également une redondance géographique ?</strong></p>

<p>Là, ça devient plus compliqué. Se posent directement les problèmes de communication entre les serveurs (qui doit être sécurisée !), le temps de basculement en cas de panne (qui doit être réduit, sinon sans intérêt), la détection de panne, le basculement des IP publiques, la gestion des serveurs qui ne sont pas identiques, etc, etc.</p>

<p>Rare sont les hébergeurs (en fait, inexistant à moins de payer 3 bras par mois, et encore) qui proposent ce genre de service, à savoir notamment une communication cryptée et des serveurs identiques dans deux lieux géographiquement redondés. A la rigueur, dans deux salles différentes ça peut être possible, et vous leur déléguez tout l'aspect sécurité/redondance/basculement, en leur faisant confiance. (Voir cet <a href="http://blog.héry.com/article12/on-rale-marre-du-filtrage-securite-des-hebergeurs">article</a>...)</p>

<p>Bon, la question ne se pose pas, je voulais tout faire moi-même, pour la gloire (et aussi pour mon porte monnaie, et également avoir une redondance de fournisseur, ça mange pas de pain).</p>

## Architecture

<p>Mon système de serveur est assez simple. Je dispose d'un serveur principal, assez costaud, permettant de faire tourner la majorité des services. Certains de ces services sont importants et devront donc faire partie du HA. D'autres le sont moins et il est acceptable de s'en passer en cas de panne matérielle</p>

<p>En secours, je dispose également d'un deuxième serveur, chez un hébergeur différent, bien moins costaud mais à même de faire tourner les quelques services importants dans un mode déprécié.</p>

<p>Ces deux serveurs n'ont aucune liaison locale (étant chez deux hébergeurs différents), présentent des composants matériels différents (ils ne sont pas identiques) et dont donc placés à des endroits géographiquement différents, dans des réseaux différents. Ils ont également leurs propres IP publiques chez chacun des hébergeurs, rendant tout fail over impossible.</p>

<p>Chacun de ces serveurs utilise Proxmox comme système d'exploitation, et tous ces fameux services sont en fait placés dans des machines virtuelles, l'hyperviseur assurant un service minimum (en gros du routage et du NAT).</p>

<p>L'idée va donc consister à mettre en place un système de haute disponibilité pour les services souhaités, entre les deux serveurs, afin qu'une panne du serveur principal ne soit pas handicapante.</p>

### Quorum

<p>Comme il est important de disposer d'un nombre impair de nodes pour permettre le fonctionnement du quorum, je rajoute à cette infrastructure un troisième serveur, qui n'héberge aucun service et sert uniquement d'arbitre. En fait, c'est une simple machine virtuelle avec Proxmox d'installé, hébergée chez moi.</p>

### Fencing

<p>Malheureusement, peu d'hébergeurs proposent un système permettant la mise un place d'un fencing. Encore moins à travers Internet. J'ai donc du choisir de me passer de fencing. <strong>Ceci est un choix qui vous attirera les foudres de la communauté Proxmox/Red Hat, et qui risque de transformer toutes vos demandes sur les forums officiels en chasse à l'homme. J'en ai pleinement conscience.</strong> Cependant, la problématique technique imposée ne me laisse pas d'autres choix. Je proposerai donc dans ces articles un moyen de "tromper" le fencing pour permettre le bon fonctionnement du HA.</p>

<p>Il est important de comprendre que c'est un choix qui peut amener un certain nombres de problématiques techniques importantes, notamment pour remonter les services après une panne. Cependant, rien d'insurmontable. Mais c'est quand même très moche. Nécessité fait loi...</p>

### Multicast

<p>Le système de cluster Proxmox nécessite une liaison multicast entre toutes les nodes. En effet, le cluster communique via ce biais entre les différentes nodes. Ce système présente l'avantage de supprimer la notion de maître/esclave présent dans Proxmox v1.X, mais est une problématique importante dans le sens qu'AUCUN réseau public n'autorise le multicast. Et surtout pas le réseau Internet.</p>

<p>Pour résoudre ce problème, j'utilise une liaison OpenVPN entre les nodes, et le cluster est construit via ce tunnel. Cela présente le double avantage de permettre facilement le Multicast sur Internet et de crypter la communication entre les nodes.</p>

<p>Cela présente le double inconvénient de remettre en place une notion de maitre/esclave (il faut un serveur VPN et des clients qui s'y connectent) et de rajouter un point de panne dans le système (si OpenVPN tombe, tout le cluster tombe). Nous verrons comment limiter ces deux problèmes.</p>

### Disque commun et réplication

<p>Un autre point important dans un cluster HA est que les services HA doivent être répliqués. En effet, si vous voulez que votre serveur de secours puisse redémarrer les machines virtuelles en cas de panne du serveur principal, et bien il faut qu'il ait les données de ces machines à disposition. De plus dans un système HA, ces données doivent être temporellement les plus proches possibles entre le serveur principal et son serveur de secours, sinon vous risquez de perdre pas mal de données lors du démarrage du serveur de secours.</p>

<p>Comme il est couteux d'avoir à disposition un disque réseau supplémentaire à partager entre les serveurs (et qu'il est de plus impossible de le faire via Internet entre deux hébergeurs différents), j'ai fait le choix d'utiliser <a href="http://fr.wikipedia.org/wiki/DRBD">DRBD</a>, un système permettant de mettre en place un RAID 1 via un réseau. Chaque action faite sur le disque du serveur principal est donc immédiatement répliquée sur le serveur de secours.</p>

<p>Ce système est plutôt robuste, mais est dépendant de la connexion entre vos serveurs. Plus votre liaison sera lente, plus le serveur de secours sera en retard par rapport au serveur principal, ce qui pose notamment problème dans le cas de base de données SQL. Cependant, la latence est minimale dans le cas d'hébergement proposant souvent au minimum 100Mo/s de bande passante.</p>

<p>Sachez tout de même qu'être trop grourmand sur le nombre de services répliqués peut entrainer des lenteurs sur vos machines virtuelles, le disque DRBD pouvant mettre en pause certains processus le temps d'écrire toutes ces données (Comme un disque classique d'ailleurs, bienvenue dans le monde des <a href="http://blog.developpeur-neurasthenique.fr/auto-hebergement-iowait-ma-tuer-1-2-vmstat-mpstat-atop-pidstat.html">I/O Wait</a>)</p>

<p>La synchronisation DRBD se fait via le tunnel Openvpn de la même façon que la communication inter-cluster.</p>

<p>Pour ma part, avec une base de données accessible à plusieurs dizaines d'utilisateurs plus une dizaine de serveurs écrivant leurs logs, je n'ai pas eu de soucis à ce niveau-là.</p>

<p><strong>Edit : Attention, et c'est une contrainte importante, utiliser DRBD impose d'avoir des KVM comme uniques machines virtuelles HA. Je n'ai pas réussi à avoir de manière satisfaisante de l'OpenVZ sur du DRBD.</strong> Il reste possible d'utiliser OpenVZ pour toutes vos autres machines non HA.</p>

### Terminologie

<p>Pour simplifier la suite, j'utiliserai les terminologies suivantes :</p>

<ul>
	<li>Serveur principal :&nbsp;<strong>Serveur principal</strong></li>
	<li>Serveur de secours :&nbsp;<strong>PRA</strong></li>
	<li>Serveur "Arbitre" :&nbsp;<strong>Arbitre</strong></li>
	<li>Haute Disponiblité : <strong>HA</strong></li>
</ul>

### Schéma

<p>Parce que c'est toujours plus visible sur un bô schéma :)</p>

<img alt="Schema infrastructure HA" src="/images/schemaHA%281%29.png" style="width: 612px; height: 353px;" />

<p>Le PRA fera serveur VPN principal, pour une raison simple : c'est lui qui est sensé tourner en permanence.</p>

<ol>
	<li>Si le serveur principal tombe, l'arbitre sera connecté en VPN sur le serveur de secours, qui aura donc le quorum immédiatement et pourra lancer le mode de secours</li>
	<li>Si l'arbitre tombe, le serveur principal aura le quorum (étant connecté au PRA), les services tourneront donc normalement</li>
	<li>Si le PRA tombe, le serveur principal devient serveur VPN secondaire. Le système de backup OpenVPN fait que l'arbitre revient se connecter sur lui plutôt rapidement. Le serveur principal retrouve donc le quorum rapidement et les services tournent normalement</li>
</ol>

<p>&nbsp;</p>

## Bon, on commence quand ?

<p>Et bien voila, je crois avoir fait le tour de présentation du système que je vais vous apprendre à monter.</p>

<p>Les tutoriels suivants expliqueront comment mettre en place le cluster via OpenVPN, comment monter la réplication DRBD et comment mettre en place le HA en lui-même.</p>

<p>Normalement, tout cela devrait tenir en 4 articles.</p>

<p>Si vous avez des questions sur ce concept ou avez besoin de détails, n'hésitez pas à me laisser un commentaire !</p>

<p>&nbsp;</p>

<p>EDIT : un petit sommaire pour avoir une idée de ce que devrait parler les prochaines articles</p>

<ul>
	<li>1/4 : Le concept, que vous venez de lire</li>
	<li><a href="http://blog.héry.com/article6/cluster-proxmox-distant-construction-du-cluster-openvpn">2/4 : Construction du cluster (openvpn), où on parlera installation, OpenVPN, personnalisation des serveurs et sécurité</a></li>
	<li>3/4 : Réplication disque (DRBD), où on parlera de l'installation de DRBD et des diverses configurations qui en découlent</li>
	<li>4/4 : Haute Disponibilité (HA), où on parlera de la configuration du HA, sa mise en place, sa gestion et l'ajout de VM à l'intérieur (fencing, cluster.conf, ...)</li>
</ul>

<p>A bientôt&nbsp;:-)</p>
