Title: Connexion SSH par clé
subtitle: (Debian/Ubuntu)
Date: 2012-08-15 10:20
Modified: 2012-08-15 19:30
Category: Système
Tags: linux,ssh automatique,debian,ubuntu, sécurité
keywords: linux,ssh automatique,debian,ubuntu, sécurité
Slug: connexion-ssh-par-cle
Authors: Victor
Status: published

[TOC]


## Pourquoi se connecter par clé ?
<p>
	Si vous êtes comme moi et que vous faites des scripts sous linux, notamment de backup, vous avez sans doute le problème d'exécuter des commandes à distance automatiquement.</p>
<p>
	Pour cela, le ssh est l'idéal; sécurisé, crypté, facilement configurable et utilisable.</p>
<p></p>
<p>
	Cependant, l'un des gros problèmes d'un script automatique, c'est qu'un mot de passe, il a un peu de mal à le taper. Alors, on peut bidouiller me direz-vous :</p>
```
echo "motdepasse" ssh root@serveurIP
```
<p>
	Pourquoi pas. Mais bon, ça oblige à mettre le mot de passe en clair, ça ne marche pas toujours selon la configuration de votre serveur ssh, et la connexion en root, c'est mal.</p>
<p></p>
<p>
	C'est là qu'intervient un système sympa de ssh :&nbsp;<strong>la connexion par clé...</strong></p>
## Le principe : rapidement
<p>
	Pour ceux qui ne sont pas trop au fait de quoi que c'est que ça, je vais rapidement rappeller le principe de se connecter à l'aide d'une clé.</p>
<p>
	En ssh, la connexion se fait en trois temps :</p>
<ul>
	<li>
		On fait une demande de connexion, le serveur renvoie une clé unique lié à sa configuration, qui va permettre de l'identifier et de chiffrer le flux (Cette demande n'est faite qu'à la première connexion)</li>
	<li>
		On accepte (ou non) cette clé. Elle permet notamment de voir si le serveur a été changé depuis la dernière connexion (si oui, attention ! potentielle attaque d'homme dans le milieu :-) )</li>
	<li>
		Ensuite, on rentre son mot de passe et la connexion s'établit</li>
</ul>
<p>
	En fait, il y a l'identification du serveur (clé du serveur), puis notre identification (via le mot de passe)</p>
<p>
	Avec une connexion par clé, c'est quasi la même chose, sauf qu'on va nous aussi utiliser une clé pour s'identifier ! De cette façon, une fois la clé installée sur le serveur, elle ne sera plus demandée et il n'y aura plus besoin de mot de passe. (trop la classe)</p>
<p>
	<em>Notez qu'on peut assortir la connexion &nbsp;par clé d'une phrase de sécurité, à rentrer lors de la connexion et qui sécurise encore le processus. Cependant, ce n'est pas intéressant pour une connexion auto donc on ne s'en servira pas ici</em></p>
<p></p>
## Génération de la clé : les choses sérieuses commencent
<p>
	Sur votre machine cliente, il va d'abord falloir générer la fameuse clé. Ou plutôt, le couple de clé, la clé publique et la clé privée.&nbsp;<a href="http://fr.wikipedia.org/wiki/Cryptographie_asymétrique">(Voir la cryptographie asymétrique)</a></p>
<p>
	Si ce n'est pas déjà fait, installez donc le client openssh :</p>
```
$ sudo aptitude update &amp;&amp; sudo aptitude install openssh-client
```
<p>
	(Bien sûr, openssh serveur est installé sur votre machine distante)</p>
<p>
	Ensuite, générez la clé pour votre client :</p>
```
$ ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
```

<p>
	Il vous faut ensuite répondre à plusieurs questions. La valeur par défaut est correcte, il suffit donc d'appuyer sur Entrée à chaque fois. Pour la passphrase, laissez vide pour qu'elle ne soit pas utilisée (c'est ce que l'on veut pour la connexion automatique)</p>
<p>
	Une fois la clé générée, un petit resumé est affiché :</p>
```
Your identification has been saved in /home/utilisateur/.ssh/id_rsa.
Your public key has been saved in /home/utilisateur/.ssh/id_rsa.pub.
The key fingerprint is:
XX:8a:XX:91:XX:ae:XX:23:XX:2e:XX:ed:XX:4e:XX:b8 utilisateur@machinecliente
```

<p>
	Les deux clés (publique et privée) sont donc stockées directement dans votre dossier home, dans un dossier caché nommé .ssh/</p>
## Envoi de la clé au serveur : tuyau crypté
<p>
	Le moment délicat est arrivé. Il vous faut transmettre votre clé au serveur, de préférence via un moyen crypté. (Bah oui, envoyer la clé en clair via ftp par exemple vous expose à vous la faire piquer, et donc potentiellement pirater...)</p>
<p>
	Pour ça, le meilleur moyen reste encore ssh ! Il va vous falloir une dernière fois votre mot de passe pour vous connecter au serveur. Grâce aux outils ssh, il suffit de faire :</p>
	
```
$ ssh-copy-id -i /home/login/.ssh/id_rsa.pub login@machineserveur
Password :
```

<p>
	Entrez votre mot de passe, et la clé sera directement copiée dans le dossier .ssh/authorized_keys de votre serveur. Ce dossier est dans le home de l'utilisateur du serveur (pas root hein ??)</p>
```
Now try logging into the machine, with "ssh 'login@machineserveur'", and check in:
  .ssh/authorized_keys
to make sure we haven't added extra keys that you weren't expecting.
```
<p>
	Une fois celà fait, vous pouvez maintenant vous connecter à votre serveur sans avoir besoin d'aucun mot de passe.</p>
<p>
	Pour testez, faites donc un petit :</p>
```
ssh login@machineserveur
```
<p>
	La connexion doit s'effectuer sans mot de passe ! Vous pouvez maintenant utiliser à loisir ssh, scp, rsync, ... dans vos scripts !</p>
## Problèmes éventuels : il peut arriver que...
<p>
	Jusqu'ici, je n'ai quasiment jamais rencontré de problèmes avec l'authentification par clé.</p>
<p>
	Cependant, si ça devait vous arriver, l'une des premières choses à vérifier est que les droits Unix sur le serveur sont corrects.</p>
### ... Le serveur continue à me demander mon mot de passe !
<p>
	C'est balot. Logguez vous sur votre serveur avec le mot de passe, et vérifiez que :</p>
<ol>
	<li>
		Les droits du dossier personnel de votre utilisateur sont corrects :&nbsp;
```
$ ls -l /home
drwxr-xr-x 5 login login 4096  3 jui.   2012 login
```
			Les droits doivent être identiques à l'exemple, c'est à dire en 755. Pour eventuellement corriger ça, faites&nbsp;
```
$ chmod 755 /home/login
```
		<p></p>
	</li>
	<li>
		Les droits du dossier .ssh sont corrects :
```
$ ls -l /home/login
drwx------ 2 login login    4096  9 sept.  2011 .ssh
```
			De même que précedemment, si les droits ne sont pas corrects, faites&nbsp;
```
$ chmod 700 /home/login/.ssh
```
		<p></p>
	</li>
	<li>
		Les droits du fichier authorized_keys sont corrects :
```
$ ls -l /home/login/.ssh/authorized_keys
-rw------- 2 login login    4096  9 sept.  2011 authorized_keys
```
			Si ce n'est pas correct, un petit :
```
$ chmod 600 /home/login/.ssh/authorized_keys
```
			devrait faire l'affaire
	</li>
</ol>
### ... Je dois utiliser un autre port que le port 22 !
<p>
	Il peut arriver que vous deviez utiliser la commande ssh-copy-id et que votre serveur utilise un port différent du port 22.</p>
<p>
	Dans ce cas, vous remarquerez que ssh-copy-id ne prend pas l'option "-p" habituellement utilisé avec ssh.</p>
<p>
	La solution est assez simple, il faut lui passer en tant qu'option ssh, de la manière suivante :&nbsp;</p>
<p></p>
```
$ ssh-copy-id -i /home/login/.ssh/id_dsa.pub '-pPORT login@machineserveur'
```
<p>
	En fait vous pouvez même passer toutes les options propres à ssh en mettant ça entre ''. (Par exemple l'algorithme de chiffrement, ...)</p>
<p></p>
### Et si c'est pas ça ?
<p>
	C'est jusqu'ici les seuls problèmes que j'ai eu en utilisant correctement tous les outils ssh. Posez la question dans les commentaires :)</p>
