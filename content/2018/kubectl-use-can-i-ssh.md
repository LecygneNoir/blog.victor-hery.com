---
Title: Kubectl : forcer l'https pour utiliser can-i
Subtitle: alias, l'http ça marche pas
date: 2018-09-14 18:32
Authors: Victor
Slug: kubectl-https-can-i
Category: Système
Tags: kubernetes, k8s, kubectl, ssh, kubectl auth, impersonate, https
keywords: kubernetes, k8s, kubectl, ssh, kubectl auth, impersonate, https
Status: published
---
[TOC]

# Le bug de kubectl auth can-i et d'impersonate

Si vous utilisez kubernetes, il est très probable que vous utilisez également kubectl pour gérer votre cluster en ligne de commande.

Dans ce cas, vous avez peut être eu besoin d'utiliser la commande `kubectl auth can-i` pour tester les différents accès de vos utilisateurs ou de vos services account.

Cette commande dispose en effet d'une option bien pratique, **impersonate**, que l'on peut appeller avec l'aide de `--as`, de cette façon :

```
$ sudo kubectl auth can-i create networkpolicy --as system:serviceaccount:default:default
yes
```

Ici par exemple, on demande si le service account *default* du namespace **default**, possède le droit de créer des NetworkPolicy dans le cluster.
Sexy, n'est ce pas !

Et bien **non** !
En effet, si par malheur votre kubectl utilise http sur le port 8080 pour discuter avec l'API kubernetes, alors cette commande ne fonctionnera pas comme attendue.
Pire ! Elle vous renverra en permanence "yes", indiquant que le compte a les bons droits, alors qu'en fait il se peut tout à fait que non...

En effet, l'API en http ne supporte tout simplement pas l'impersonate. Sauf que kubectl, à cause d'un bug, ne gère pas le fait de vous le dire.

En tout simplicité, toutes les commandes que vous lancerez avec `--as` seront donc réellement lancées avec votre utilisateur admin actuel :-)

Ceci est donc un petit pense-bête avec contexte pour garder la manip sous le coude !

# kikekoi qu'on fait ?

Pour régler ce problème, il devient donc nécessaire de parler à l'API de kubernetes en https, et heureusement ça peut se configurer !

Pour ce faire, on va assez simplement utiliser le fichier admin.conf qui est usuellement généré lors de l'installation du cluster.

Dans une configuration standard, le fichier devrait être placé dans `/etc/kubernetes/admin.conf`.
Il se peut également qu'il soit placé dans `/root/.kube/config`.

Dans tous les cas, vous pouvez l'appeler de la manière suivante :

```
sudo kubectl --kubeconfig /etc/kubernetes/admin.conf auth can-i create networkpolicy  --as system:serviceaccount:default:default
no
```

Et voila, on obtient les bons droits avec un impersonate fonctionnel ;-)