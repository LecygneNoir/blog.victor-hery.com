Routeur 4G lvl 1
#################
:subtitle: Boostez votre connexion à la maison !
:date: 2018-01-31 10:00
:authors: Victor
:slug: Routeur-4g-lvl1
:category: Réseaux
:tags: 4G, routeur, hébergement, do it yourself, tethering
:keywords: 4G, routage, hébergement, do it yourself, tethering
:status: published
:summary: Boostez votre connexion à la maison !

.. contents::

On veut tous avoir la connexion la plus rapide possible. Parce que les Mbps, ça fait paraitre plus grand, ça fait revenir l'être perdu, ça ramène l'amour à la maison.

Mais quand on est paumé dans une cambrousse qui dispose d'une connexion méritant à peine le A de ADSL, on souffre, chaque jour, en silence, dans une indifférence générale.  
On ne reviendra donc pas sur le déploiement de la fibre en France, ce n'est pas le sujet du jour.

Aujourd'hui, on va voir comment pouvoir peut être gratter quelques octets de bande passante, ou multiplier votre connexion par 10, ça dépend du déploiement 4G dans votre cambrousse :-D

Merci à Fabien pour ses conseils sur le matériel et son expérience sur le sujet !

Matériel
=========

Pour cet article, il va falloir du matériel physique, pas de miracle.

Il vous faudra au minimum :

- un routeur avec 2 ports RJ 45 (Un wan et un lan) et un port USB (pour la clef 4G). Je vous conseille d'en prendre 2 pour en avoir un en secours en cas de panne
- une clef 4G. Il est conseillé de prendre une clef d'une marque faisant également des téléphones, ce afin de limiter les chances que votre opérateur détecte une utilisation en mode modem (souvent interdite par les CGU)
- une carte SIM avec un forfait 4G
- les câbles RJ 45 qui vont bien pour brancher le routeur

Vous pouvez aussi prendre une antenne 4G pour renforcer votre signal si vous voulez, ça mange pas de pain.

.. figure:: {static}/images/routeur4G/routeur_antenne.jpg
    :width: 400px
    :alt: antenne et routeurs

    Les 2 routeurs et leur antenne

Dans ce tutoriel, voici le matériel que je vais utiliser. Il est minimaliste mais robuste et reconnu pour fonctionner, donc je me base dessus.

- Routeur `NEXX WT3020`_.
- Clef 4G `Huawei E3372`_. Pour ce modèle, je vous conseille ebay, on en trouve à des prix excellent, certains FAI les fournissant comme clef 4G officielle
- Antenne `Lafalink ANT4G01`_. Un peu massive, mais une bonne longueur de cable et qui fait bien le job

Cela étant dit, comme on va utiliser `OpenWRT`_ pour la partie logicielle, n'importe quel routeur qui le supporte devrait fonctionner ;-)


Installation du routeur
=======================

OpenWRT
-------

Cette partie va concerner l'installation du routeur `NEXX WT3020`_ sous OpenWRT_. La procédure devrait être sensiblement la même pour la plupart des modèles de routeurs, mais n'hésitez pas à vous reporter à la documentation OpenWRT en cas de doute.

Je précise également qu'à ma connaissance, ce routeur ne dispose d'aucune backdoor matérielle connue, donc une fois OpenWRT installé, vous devriez être tranquille. Cependant, si jamais vous avez connaissance d'une telle faille, n'hésitez pas à me le faire savoir !

Connectez votre routeur à votre ordinateur de manière à ce que votre PC soit côté LAN du routeur, et branchez un câble relié à Internet sur la partie WAN. Le routeur doit avoir Internet, et vous devez pouvoir vous connecter au routeur :

.. figure:: {static}/images/routeur4G/IMG_20180117_101447.jpg
    :width: 400px
    :alt: Le WT3020 connecté

    Le WT3020 connecté

Dès que la connexion au WT3020 est établie, on peut se connecter en telnet. Par défaut, celui-ci vous donne une IP en 192.168.1.0/24, et lui-même est sur le 192.168.8.1.

Les identifiants par défaut sont ci-dessous :

.. code-block:: bash

	$ telnet 192.168.8.1
	Trying 192.168.8.1...
	Connected to 192.168.8.1.
	Escape character is '^]'.

	(none) login: nexxadmin
	Password: y1n2inc.com0755


On va ensuite récupérer l'image openwrt. Pensez à aller vérifier si c'est bien la dernière en date ;-)

.. code-block:: bash

	# cd /tmp
	# wget http://downloads.openwrt.org/chaos_calmer/15.05/ramips/mt7620/openwrt-15.05-ramips-mt7620-wt3020-8M-squashfs-sysupgrade.bin
	# mtd_write -r write openwrt-15.05-ramips-mt7620-wt3020-8M-squashfs-sysupgrade.bin mtd3
	# reboot

Le routeur va redémarrer, et vous devriez tourner sur OpenWRT_

First conf
----------

La première chose que l'on va faire, c'est dégager telnet et configurer ssh.

Pour cela, on va se reconnecter une dernière fois en telnet. **Notez que OpenWRT par défaut utilise 192.168.1.0/24 comme réseau.**

.. code-block:: bash

	$ telnet 192.168.1.1
	Trying 192.168.1.1...
	Connected to openwrt.lan.
	Escape character is '^]'.
	=== IMPORTANT ============================
	Use 'passwd' to set your login password
	this will disable telnet and enable SSH
	------------------------------------------

Comme l'indique si gentiment le motd, il suffit de configurer un mot de passe à l'aide de ``passwd`` pour activer le SSH et désactiver telnet, il n'y a plus qu'à :

.. code-block:: bash

	root@OpenWrt:/# passwd
	Changing password for root
	New password: 
	Retype password: 
	Password for root changed by root
	root@OpenWrt:/# 


A partir de là, telnet va être désactivé. C'est le moment de se connecter en SSH pour vérifier que vous n'avez pas d'erreurs dans le mot de passe !

Si tout est ok, vous pouvez également commencer à utiliser l'interface web LuCi via un navigateur, avec les accès root identiques au SSH.

Configuration avancée
---------------------

Une fois le routeur installé, je vous inviteà  vous connecter sur l'interafce web, via votre navigateur, en tapant son adresse IP : http://192.168.1.1

Les accès sont les même que ceux que vous avez rentrés pour configurer le SSH : utilisateur root, avec le mot de passe associé.

Le routeur est déjà plutôt fonctionnel à l'installation, néanmoins je vous propose quelques modifications qui permettent d'éviter les désagrément (type intrusion) en désactivant des services pas forcément utiles.

Onglet System :

- Configurez le NTP avec votre timezone dans le menu System
- Dans le menu Administration, indiquez que vous souhaitez écouter en SSH uniquement sur l'interface LAN. Eventuellement vous pouvez indiquer une clef ssh publique si vous en avez une, ça peut faciliter les connexions ;-)

Onglet Network

- Au niveau des Interfaces, vous devriez avoir une Interface LAN branchée (en vert), et une interface WAN débranchée (en rouge). Les couleurs indiquent les zones du pare-feu, le vert étant la zone "sécurisée", le rouge la zone "non sécurisée", c'est à dire Internet. On rajoutera plus tard une interface pour la clef 4G. C'est également ici (en éditant chaque interface) que vous pouvez modifier l'adresse du routeur si 192.168.1.1 ne vous convient pas.
- Onglet wifi : choisissez si vous souhaitez ou non vous servir du wifi du routeur. Si vous voulez que vos matériels en wifi profitent de la 4G, il est préférable de l'activer. N'oubliez pas de mettre une clef de sécurité !

Le reste est inutile pour l'instant, mais vous pouvez parcourir les menu pour jouer un peu si vous le souhaitez ;-)


Clef 4G
=======

Il va donc être temps de brancher votre clef 4G et de la configurer. Bien entendu, mettez la carte SIM préalablement ^^

Drivers openwrt
---------------

Openwrt n'a pas de base les drivers nécessaires pour reconnaître les périphériques réseaux en USB on va donc devoir installer les paquets qui vont bien.

Connectez vous en SSH au routeur et installez les paquets : 

.. code-block:: bash

	opkg update
	opkg install kmod-usb-net kmod-usb-net-rndis kmod-usb-net-cdc-ether usbutils udev

Note :
- le paquet kmod-usb-net-cdc-ether va permettre de reconnaître la majorité des clefs USB du marché
- le paquet kmod-usb-net-rndis de son côté est utile si vous souhaitez utiliser votre Smartphone en usb plutôt qu'une clef

Vous pourrez trouver plus d'information sur le tethering `ici`_.


Interface réseau
----------------

Avant tout chose, il va falloir indiquer au routeur quoi faire de ce nouveau périphérique réseau.

Connectez votre clef 4G, puis connectez vous en ssh au routeur.

La commande 

.. code-block:: bash

	ip a 

devrait vous lister les interfaces réseaux disponibles. Il va falloir retrouver celles qui correspond à la clef. 

Chez moi, j'ai eth0 qui correspond au lan, eth0.2 qui correspond au wan, et la clef est apapru en tant que eth1. Servez vous de l'IP pour la reconnaitre, traditionnellement les clefs 4G ont un DHCP sur le réseau 192.168.8.0/24 :-)

A défaut, reportez vous au mode d'emploi de la clef ou à son emballage pour récupérer l'adresse MAC.

Dans le cas d'un smartphone, l'interface est souvent vue comme **usb0**.

Une fois l'interface récupérée, on va aller la configurer sur le routeur. Retournez donc sur l'interface web du routeur !

Dans l'onglet Network -> Interfaces, on va créer une nouvelle interface grâce au bouton "Ajouter une interface" en dessous des interfaces existantes :

.. figure:: {static}/images/routeur4G/Add_Interfaces_4G.png
    :width: 400px
    :alt: ajout d'une interface sur LuCi

    Ajout de l'interface 4G0 sur LuCi


J'ai pour ma part nommée l'interface 4g0, mais vous pouvez choisir n'importe quel nom qui vous parle.

Une fois l'interface ajoutée, il va être important de l'éditer immédiatement, pour aller dans l'onglet "Pare-feu" de l'interface et la configurer comme étant dans la zone rouge "wan". De cette manière, le routeur considérera que c'est Internet qui est derrière, et bloquera la plupart des intrusions par défaut.

Je vous conseille également d'en profiter pour lui caler une IP fixe, histoire de faciliter la gestion.

Une fois ces configurations faites, vous pouvez débrancher et rebrancher la clef 4G pour lui faire redémarrer ses services et récupérer le réseau.

Configuration de la clef
------------------------

Pour cette deuxième partie, il est préférable que le routeur soit branché à votre PC directement (toujours via sa prise LAN), car la plupart des clefs 4G vont générer une popup vous permettant (entre autre) de rentrer le code PIN de votre SIM.

Si ce n'est pas le cas, il faudra se référer au mode d'emploi de la clef.

A défaut, vous devriez pouvoir vous connecter à la clef via votre navigateur, en rentrant son IP. Par exemple : http://192.168.1.1

Ici, la configuration va entièrement dépendre de votre clef, je vais donc parler de la clef `Huawei E3372`_, mais vous pouvez vous en servir comme liste de points à vérifier :

- Configurez le code PIN de votre carte SIM, en cochant la case permettant à la clef de s'en souvenir. Ainsi, même en cas de redémarrage du routeur, coupure electrique, etc, la carte sim sera automatiquement réactivée (et il reste peu probable qu'on vienne vous piquer le routeur avec le PIN enregistré dans votre salon)
- Onglet Settings -> Mobile Connection : Vous devez "Turn ON" la connexion mobile, qui est désactivée par défaut. Je vous conseille cependant de laisser le roaming désactivée, sauf si vous avez un forfait en béton. N'oubliez pas de cliquer sur "Apply"
- Onglet Settings -> Security -> DMZ Settings : Vous pouvez laisser le routeur OpenWRT gérer la partie pare-feu en indiquant ici son IP, pour que la clef redirige tout le traffic entrant vers lui. Principalement utile si vous souhaitez faire de l'hébergement de partie de jeu en ligne par exemple, même si vous n'aurez (sans doute) pas d'IP fixe via la 4G.
- Onglet Settings -> Security -> UPnP : Je vous conseille de désactiver l'UPnP, qui est une source de faille trop importante et est très peu utile sur votre clef dans ce contexte

Détection du tethering
----------------------

La plupart des FAI se servent d'une technique de détection du `tethering`_ pour limiter les usages en mode modem de leurs forfaits. C'est au moins le cas de Free et Bouygues.

Cette détection est indépendante du type de matériel (je conseille ici encore de prendre une clef chez un constructeur faisant aussi des téléphones), mais leur permet d'ajouter des règles pour limiter le débit en cas de doute.

Typiquement, cela m'est arrivé avec la clef Huawei, qui se retrouvait limitée après 3 minutes de fonctionnement, et la limitation sautait si je la redémarrait (avant de revenir quelques minutes plus tard)

Cette technique se base sur le `Time To Live`_ (TTL) de vos paquets, en partant du principe que si vous êtes derrière un routeur 4G, alors celui-ci va décrémenter ce TTL (ce qui est normal), et donc le FAI verra arriver des paquets avec un TTL inférieur à celui par défaut.

Or, ce TTL est en général de 64 ou 127, selon les différents OS, il est donc aisé de se dire "ah, je vois le paquet arriver, le TTL est de 62 (ou 125), donc il y a eu 2 sauts entre le client et moi, il utilise un routeur 4G !".

On va donc faire en sorte que notre routeur remette ce TTL à une valeur acceptable avant d'envoyer les paquets au FAI :-)

Pour cela, iptables est notre ami !

Connectez vous sur l'interface web d'OpenWRT, et rendez-vous dans l'onglet Network -> firewall -> Custom rules

Rajoutez à la fin du fichier une règle iptables comme suit, en utilisant l'interface réseau de la clef 4G utilisée plus haut :

.. code-block:: bash

	 iptables -t mangle -I POSTROUTING -o INTERFACE_4G -j TTL --ttl-set 66

Et bien sûr sauvegardez.

Cette règle va faire en sorte que chaque paquet sortant du routeur sur l'interface de la clef 4G verra son TTL forcé à 66, quelque soit sa valeur précédente. De fait, le FAI verra arriver un paquet avec un TTL plus grand que ce qu'il détecte, et ne devrait plus vous brider ;-)

Tests et sauvegardes
====================

Une fois ces configurations appliquées, vous devriez pouvoir joindre Internet à travers la clef sans problème.

Déconnectez votre PC de toute autre sources de réseau (cables, wifi, ...) et testez de naviguer un peu. Lancez quelques `Speed test`_ pour voir si la connexion à de la patate.

Si tout est ok, je vous conseille fortement d'aller sauvegarder les configurations du routeur **et** de la clef 4G via les menu backups correspondants.

Ainsi si le matériel décède, plante, ou quoique ce soit, vous serez bien content d'avoir ces fichiers à restaurer pour repartir rapidement avec du matériel neuf ;-)

Pour OpenWRT, la sauvegarde se trouve dans l'onglet System -> Backup

Pour la clef Huawei, la sauvegarde se trouve dans Settings -> System -> Backup


Conclusion
==========

Il ne vous reste plus qu'à brancher votre routeur là où vous le souhaitez pour profiter de la 4G ! 

Est ce que vous l'utilisez plutôt à la place de votre box ? Associé à la box ? Derrière un switch pour que toute la maison profite de la 4G ? N'hésitez pas à me laisser un petit commentaire pour partager votre configuration ! ;-)

Dans les prochains articles, nous verrons comment utiliser de manière fun votre connexion ADSL en plus de la 4G.

En outre, les réseaux 4G étant notoirement surveillés (encore plus que l'ADSL), voir non-neutres vers certains services (coucou Free vs Youtube), nous verrons comment rajouter un brin de VPN pour aider un peu.


Bilbiographie et liens
======================

- Routeur `NEXX WT3020`_.
- Clef 4G `Huawei E3372`_.
- Antenne `Lafalink ANT4G01`_.
- `How To Turn a WT3020 into a TOR router`_.
- Le wiki `OpenWRT LuCi`_.
- La documentation mwan3_.
- `Comment modifier le TTL de sa connexion`_ pour limiter le tethering

.. _How To Turn a WT3020 into a TOR router: http://www.securityskeptic.com/2016/01/how-to-turn-a-nexx-wt3020-router-into-a-tor-router.html
.. _Lafalink ANT4G01: http://www.lafalink.com/product_519.html
.. _Huawei E3372: http://consumer.huawei.com/en/mobile-broadband/e3372/specs/
.. _NEXX WT3020: https://wiki.openwrt.org/toh/nexx/wt3020
.. _OpenWRT: https://fr.wikipedia.org/wiki/OpenWrt
.. _OpenWRT LuCi: https://github.com/openwrt/luci/wiki
.. _mwan3: https://wiki.openwrt.org/doc/howto/mwan3
.. _Speed test: http://www.speedtest.net/
.. _ici: https://wiki.openwrt.org/doc/howto/usb.tethering
.. _Time To Live: https://fr.wikipedia.org/wiki/Time_to_Live
.. _Comment modifier le TTL de sa connexion: https://lafibre.info/4g-bytel/optim-ttl/
.. _tethering: https://fr.wikipedia.org/wiki/Modem_affili%C3%A9