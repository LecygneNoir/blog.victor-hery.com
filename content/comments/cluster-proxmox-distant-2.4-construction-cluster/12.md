email: courriel@victor-hery.com
date: 2013-09-20T14:17+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
slug: 12md
replyto: 11md

Bonjour,

Pour répondre à tes questions :   
1) Avec le système DRBD, la réplication se fait en temps réel, car DRBD permet de faire un système de RAID 1, mais via le réseau. Du coup, tout ce qui est écrit à droite est immédiatement écrit à gauche, à la latence prêt.  
Ici, dans le cadre d'un cluster distant, la réplication se fera comme si on utilisait un disque de vitesse égale à la bande passante (pour simplifier). Il est donc préférable de ne pas mettre de machines trop gourmandes en I/O. Pas de souci par contre pour une configuration de ce type sur un réseau local (en gigabits)  

2) L'idée du HA est que la machine soit basculée sans coupure. Dans les faits avec ce système, c'est effectivement le cas, la machine bascule sans se rendre compte qu'elle a changée de côté. Il y a par contre forcément une latence pour la joindre (le temps que les ARP se recalent). Cette latence est de nouveau beaucoup plus limitée en cas de configuration sur un réseau local, et quasi nulle si on peut disposer d'IP failover.

3) Du coup, découlant de mes réponses au dessus oui, on peut tout à fait faire ça avec un câble en direct :)  
C'est même beaucoup plus performant, et on peut se passer de toute la partie OpenVPN puisque le réseau est direct (l'intérêt de l'OpenVPN étant principalement de donner la possibilité de faire du multicast sur Internet de manière propre, et de chiffrer la discussion DRBD). Par contre du coup, ce qui t'intéresserait plus, c'est la config du DRBD, qui est la suite de ces articles (Et qui arrivera un jour, je ne désespère pas ! :s)

A plush'

Victor
