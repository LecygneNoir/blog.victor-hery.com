email: neh.mobile@gmail.com
date: 2014-01-21T23:20+01:00
author: Blotto
slug: 20md
website:

Slt

@Victor et pour les autres qui ont galeré comme moi !!!

Le problème provient du proxy qui ne peut pas valider un certificat autosigné, il faut lui indiquer manuellement l'emplacement du certificat pour qu'il puisse effectuer la connexion.

Modèle :

client---https---proxy---https---serveur

Copie le certificat SSL du serveur sur le Proxy via SCP et ensuite ajoute ceci a ton virtualhost.

    SSLProxyEngine On
    # Pointe le certificat directement
    SSLProxyCACertificateFile /etc/apache2/ssl/serveur.crt

ou

    SSLProxyEngine On
    # Pointe le dossier du certificat
    SSLProxyCACertificatePath /etc/apache2/ssl/

Marche impec sur Proxmox 3.1
