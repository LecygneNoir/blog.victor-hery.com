email: courriel+blog@victor-hery.com
date: 2017-12-05T09:42+01:00
author: Victor
website: https://blog.victor-hery.com/
slug: 3md
replyto: 0md

Hello Nicolas,

Concernant les "limitations" de LXC, précisons de suite que ça fait 1 an minimum que je n'y ait pas touché, il est probable que des choses aient changées :-D
Au rayon de ce qui me gênait, il y avait l'exploitation "en masse" de container LXC, c'est à dire créer et gérer plusieurs container. LXD a amélioré les choses même s'il n'est pas trivial à prendre en main, et j'imagine que Proxmox a aussi apporté une belle pierre à cet édifice, mais je n'ai pas testé leur version 5 avec LXC

Il y avait surtout l'étanchéité entre l'hyperviseur et les container qui étaient assez peu efficace. Au début, les CT pouvaient même voir les processus lancés par l'hyperviseur, mais ça a été modifié depuis il me semble ^^
Openvz apporte surtout ça : des limitations utiles à la revente, telle que la limitation IO (sans prix quand un CT se fait poutrer !), la limitation de bande passante, une gestion fine des ressources RAM/CPU, etc.
Il se peut que ça ait été implémenté depuis avec les cgroups, et que Proxmox ait également apporté un certain nombre de chose à ce sujet, mais dans mes souvenirs ça restait assez peu efficace.

Le principale avantage pour moi de LXC est plutôt pour les tests : bien intégré au kernel, on peut lancer rapidement un CT pour jouer avec et tester des trucs (plus simple que Docker je trouve pour le coup), etc

Il faudra que je me remette à la doc un de ces jours !

A plush'
