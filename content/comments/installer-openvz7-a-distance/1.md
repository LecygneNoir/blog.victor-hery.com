email: courriel+blog@victor-hery.com
date: 2017-12-04T08:15+01:00
author: Victor
website: https://blog.victor-hery.com/
slug: 1md
replyto: 0md

Hello Nicolas,

Merci pour ton commentaire, et heureux si je peux aider !
Proxmox, excellent choix :-)
Ils ont abandonnés openvz7 pour LXC sur leur dernière version, certaines choses manquent pour moi sur LLXC pour le "production/revente" (notamment niveau limitation des ressources), mais ça reste un très très bon outil, pour le coup bien plus simple et intégré qu'openvz, ces derniers ayant fait certains choix assez douteux sur leur version 7, faut bien l'avouer..

En tout cas, si Proxmox utilise toujours Debian attention, cet article est plus destiné à centos. Notamment, Debian n'utilise pas dracut pour l'initramfs, donc la configuration de initrd est un peu différente.
Je t'invite à regarder l'article que j'ai mis en source : [Stockage chiffré intégral sur serveur distant Debian](https://blog.imirhil.fr/2017/07/22/stockage-chiffre-serveur.html) et qui concerne Debian :-)

Bon courage, et n'hésite pas à me contacter si tu as des questions/des soucis !

A plush'
