email: courriel+blog@victor-hery.com
date: 2020-01-17T08:02+01:00
author: Victor
website: https://blog.victor-hery.com/
slug: 1md
replyto: 0md

Bonjour Robin !

Merci pour l'info sur le fait qu'ils ont enlevé la commande ceph-disk. Il me semblait qu'ils gardaient un wrapper, il va falloir que j'essaye d'installer un proxmox 6 pour tester si je veux mettre à jour l'article 😅

Concernant votre problème, au vu de la commande je pense que le souci vient du fait que vous n'avez pas modifié le premier 5: dedans

En effet même si la syntaxe n'est pas très intuitive je l'avoue, il y a bien quatre `5:` à changer. Plus précisément, tous les chiffres `X:` doivent être identiques pour chacune des options, or dans votre commande le premier est rester en `5:`.

Cela a pour conséquence que la commande doit créer une partition 5 de 100M, puis essayer de renommer la 4 qui n'existe pas, d'où l'erreur.

Vérifiez si la partition 5 n'existe pas avec fdisk, auquel cas supprimez là, puis essayez de relancer la commande de cette manière :

```
sgdisk --new=4:0:+100M --change-name="4:ceph data"   --partition-guid=4:$(uuidgen -r)   --typecode=4:4fbd7e29-9d25-41b8-afd0-062c0ceff05d -- /dev/sdb
```

Notez le `--new=4:0:+100M` avec un `4:` :-)

N'hésitez pas à me dire si jamais le souci se pose toujours !

Bon courage
