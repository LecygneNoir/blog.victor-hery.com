email:
date: 2019-09-11T10:56+02:00
author: zwindler
slug: 0md
website: https://blog.zwindler.fr

Merci pour l'article, bien documenté avec les playbooks et les explications :)  

Plutôt qu'utiliser OpenVPN (justement à cause le la limitation client/serveur), as tu envisagé une solution avec un VPN multipoint (type tinc ou zerotier) ?  

Et même si je suis un grand fan d'ansible moi aussi, j'ai essayé récemment Rook qui "simplifie" l'instanciation du cluster Kubernetes en ajoutant des CRDs qui gèrent tout à ta place. A voir comment ça se comporte dans le temps mais ça simplifie pas mal la gestion quotidienne je trouve.
