email: courriel+blog@victor-hery.com
date: 2018-02-27T09:02-05:00
author: Victor
slug: 1md
replyto: 0md

Not really, as here I edit manually the file, so variables will no be automatically replaced.  
I use uppercase to avoid confusion, apparently not with heavy success :-/

You could use variables with some ``echo`` or ``cat << EOF`` tricks, but in this case all the file is crushed, and there are some very interesting documentation inside the example file I would avoid to destroy.

Of course, the perfect way would be to use some deployment tools like Ansible to deploy the server and edit configuration file, but this is out of scope ^^

Thank you!