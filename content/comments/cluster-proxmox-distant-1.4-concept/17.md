email: bentien@live.fr
date: 2013-04-17T15:35+01:00
author: Ozzi
slug: 17md
website:

Bonjour,  
Tout d'abord merci pour votre explication très claire des notions de quorum et de fencing et pour votre article en général !   
Vous dites qu'utiliser drbd avec openVZ n'est pas réalisable, est-ce vraiment le cas ? Je suis actuellement le tuto de nedProduction que vous citez au début de l'article et il me semble que toutes ses VM sont en openVZ lorsqu'il met en place drbd.  
D'ailleurs pensez vous bientôt publier votre 3ème article dans lequel vous aborderez drbd ? :)
