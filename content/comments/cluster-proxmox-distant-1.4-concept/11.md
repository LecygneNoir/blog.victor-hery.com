email: courriel@victor-hery.com
date: 2013-02-10T13:05+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
slug: 11md
replyto: 4md

Hello,

Désolé, c'est vrai que je pense que les commentaires ne sont pas un support très appropriés pour une discussion technique de ce type. Les courriels sont plus pratiques, et permettent plus d'espace de discussion. Surtout que j'ai tendance à être très verbeux ^^

Pour l'instant, la discussion avec Gégé semble porter sur un problème de fencing. Le système marche bien en cas de coupure réseau ou de tests manuels, mais le fencing n'est plus efficace lors d'une coupure de courant. En effet l'interface permettant le fencing n'est plus alimentée non plus, et donc impossible de s'en servir.  
Du coup les nodes encore allumées tentent en boucle de fencer la node morte, sans succès.   
Dans ce genre de cas, les machines en HA ne sont jamais relancées, puisque tout les services cluster sont bloqués même si les nodes ont le quorum. Une solution est alors effectivement de configurer un fencing 'human' pour pallier ce défaut, mais du coup adieu l'automatisme. Pour gégé, il faut essayer de trouver une solution avec une alimentation de secours, voire un onduleur permettant de donner le temps au fencing d'agir :)

Pour ton problème de DRBD, et dans le cas particulier de Proxmox, il est bien plus simple de mettre en place un disque master/master. ATTENTION ! Il faut que le disque ne soit utilisé que d'un côté à la fois ! Typiquement, prévoir un disque DRBD pour les machines de la node A, et un disque DRBD pour les machines de la node B. En master/master, dès l'activation du HA les machines basculées ont les droits d'écriture et peuvent démarrer. Mais il est impératif que toutes les machines tournant sur le disque DRBD basculent d'un coup. Si certaines tournent à gauche et d'autres à droite, ça peut poser de gros problèmes. (Split brain, comme on dit)  
Dans tous les cas et dans le cadre de ce tuto, le basculement est prévu pour être automatique, mais pour remonter le cluster, il faudra y aller à la main, principalement pour cette raison : il faut dire à DRBD quelle node possède les bonnes données et quelle node doit voir ses données écrasées par une synchro.

Dans le cas où tu n'utilises pas l'ISO Proxmox, et tout particulièrement si tu n'utilises pas LVM pour ta partition système/ton swap, il est possible dans le cluster.conf de définir des "failover domains" qui gèrent les ressources, donc DRBD et LVM. Dans ce cas, tu peux activer le mode master sur un disque DRBD en cas de déclenchement de HA, et tu peux également utiliser CLVM (Clustered LVM), une extension de LVM qui permet de gérer la synchronisation sur DRBD de manière très sympathique (notamment la gestion de qui a le droti d'écrire à quel moment, et qui a raison). Cependant, utiliser ces techniques demandent que LVM soient lancé par les processus de cluster uniquement une fois que les nodes ont démarrées, ce qui est impossible dans le cas de l'ISO Proxmox, puisque la partition système est sur LVM.  
Cette technique de CLVM semble permettre plus facilement l'utilisation d'OpenVZ sur DRBD également.  

J'ai choisi dans ces tutos d'utiliser l'ISO Proxmox car son partitionnement LVM se prête bien à la gestion des disques DRBD. Je n'utilise donc pas CLVM, et mes disques DRBD sont en master/master, ce qui pose au final relativement peu de problèmes, étant prévu pour fonctionner en mode Serveur principal/Serveur de secours (Aucune VM ne tournant donc sur le disque DRBD du serveur de secours)

Voila, j'espère avoir pu vous aider :)
