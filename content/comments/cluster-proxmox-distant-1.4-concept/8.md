email: courriel@victor-hery.com
date: 2013-02-04T13:43+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
slug: 8md
replyto: 4md

Hello,

Si tu veux, je t'invite à mettre ton adresse courriel dans tes commentaires, elle ne sera visible que par moi, et ça sera plus pratique pour discuter :)  
(En attendant que je répare mon formulaire de contact ^^")

Pour ce qui est des logs, il semble bien que c'est le fencing qui pose problème.  
la machine-proxmox1 (qui est node 2 si je lis bien) disparait, et donc machine-proxmox2 essaye le fencing.  
On voit bien qu'il échoue en boucle, dans ce cas là, aucun service HA ne se lancera. En effet, le cluster considère qu'il ne peut pas être sûr que la node est vraiment "morte", et donc que peut être les VM tournent encore dessus. Pour éviter des corruptions sur le disque de données synchronisé (DRBD, NFS, SAN, ...), le cluster ne peut pas relancer les VM en HA tant qu'il n'est pas sûr que l'hôte est down.  

Après, si ton fencing marche quand tu débranches des câbles, et pas en cas de coupure de courant, il faudrait peut être brancher ta carte ILO sur une alimentation séparée ? Voir avec un onduleur séparé ?  
Est ce que en lançant un fencing à la main avec le fence_agent ça fonctionne bien ? Même si le courant est coupé ?  

En tout cas, ton problème semble venir de là :)

Bon courage

A plush'

Victor
