Title: Sonde RIPE Atlas :
subtitle: ça clignote ou ça clignote pas ?
Date: 2016-01-18 22:52
Modified: 2016-01-18 22:52
Category: Réseaux
Tags: linux, ripe atlas
keywords: linux, ripe atlas
Slug: sonde-ripe-atlas.clignote-ou-pas
Authors: Victor
Status: published

[TOC]

## La sonde RIPE Atlas

<p>Depuis quelques mois, je participe au programme <a href="https://atlas.ripe.net/">RIPE Atlas</a>, un programme extrêmement intéressant du RIPE NCC qui consiste à installer chez des particuliers ou des professionnels des petites sondes permettant de tester la qualité du réseau, effectuer des tests de connectivité, etc.</p>

<p>L'objectif est de construire un réseau pour collecter des données sur la qualité de la connexion dans le monde entier, et ainsi avoir une carte détaillée à l'échelle de la planète de l'état du réseau Internet.</p>

<p>A peu près n'importe qui peut adhérer à ce programme, même si en France il devient difficile d'en avoir vu la quantité de participants...</p>

<p>Ici nous allons discuter de comment réparer une sonde Atlas de 3eme génération qui ne veut plus se connecter. Cette astuce me vient du support RIPE Atlas, et est publiée avec leur aimable accord :-)</p>

## Diagnostic

<p>Il y a quelques temps, ma sonde s'est mise à refuser de se connecter sans raisons évidentes.</p>

<p>(Bon, réellement suite à une coupure de courant nocturne, mais bon, passons ^^)</p>

<p>Quoiqu'il en soit, un diagnostic basique montrait que la sonde démarrait, qu'elle récupérait bien une adresse IP en DHCP, IPv4 et IPv6, elle effectuait même des requêtes DNS.</p>

<p>Mais, elle ne se connectait pas jusqu'aux serveurs RIPE Atlas, qui la voyait comme absente, et le clignotement des diodes sur le dessus ne correspondait à aucun des schémas recensés dans la <a href="https://atlas.ripe.net/about/faq/#what-do-the-lights-on-the-side-of-the-probe-mean">FAC</a>.</p>

<p>A partir de là, n'ayant aucun accès possible à la sonde, il devient difficile d'aller plus loin, mais heureusement le support RIPE est là. Et il est très efficace, même en période de fêtes !</p>

## L'astuce

<p>L'astuce est en fait assez simple pour remettre à zéro la sonde et lui permettre de se reconnecter comme si rien ne s'était passé :</p>

<ol>
	<li>Éteindre la sonde</li>
	<li>Retirer le plug USB qui est enfiché dans la sonde</li>
	<li>Démarrer la sonde et connectez là comme habituellement</li>
	<li>Attendez pendant 5 à 6 minutes</li>
	<li>Remettez en place le plug USB à chaud, sans éteindre la sonde</li>
	<li>Au bout d'une dizaine de minute, la sonde a détecté de nouveau le plug et lancés les scripts adéquats.</li>
	<li>Le clignotement revient à la normale, et la sonde se reconnecte</li>
	<li>Et voila</li>
</ol>

<p>Simple et efficace.</p>

<p>Merci au RIPE pour ce projet et pour l'astuce !</p>
