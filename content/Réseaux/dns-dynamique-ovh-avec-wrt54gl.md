Title: Utiliser un DNS dynamique chez OVH avec un WRT54GL sur tomato
Date: 2012-09-11 21:16
Modified: 2012-09-11 21:16
Category: Réseaux
Tags: DynDNS OVH, DNS dynamique, tomato, WRT54GL
keywords: DynDNS OVH, DNS dynamique, tomato, WRT54GL
Slug: dns-dynamique-ovh-avec-wrt54gl
Authors: Victor
Status: published

[TOC]

## Oui, le titre est long, et alors ?

<p>Si on faisait toujours des titres courts, ça serait pas référencés sur Google ! (#oupas)</p>

<p>Bon, cet article est sérieux, attaquons.</p>

<p>&nbsp;</p>

<p>Dernièrement, j'ai vu que OVH, en tant que <a href="http://fr.wikipedia.org/wiki/Registre_de_noms_de_domaine">registrer</a> (chez qui on achète ses noms de domaine), proposait de configurer directement des entrées DNS Dynamique.</p>

<p>Option plutôt sympathique il faut l'avouer, ça permet de gérer facilement ses noms de domaine avec IP dynamique. Quelques limitations sont à prévoir, notamment au niveau de la mise à jour de l'enregistrement. On est là pour en parler&nbsp;:-)</p>

<p>&nbsp;</p>

## C'est quoi un DNS Dynamique ?

<p>Un DNS dynamique, c'est en fait un DNS classique, mais avec un temps de réplication et de cache (<a href="http://fr.wikipedia.org/wiki/Time_to_Live">TTL</a>) très court.</p>

<p>Ceci, associé à un logiciel spécifique installé chez le client, permet d'avoir un nom de domaine qui se met à jour très rapidement à l'échelle mondiale. Cela permet notamment de gérer facilement lorsque vous avez besoin de joindre quelque chose chez vous (NAS, PC en VNC, auto-hébergement, ...) et que vous avez un FAI qui ne vous donne qu'une IP dynamique, c'est à dire qui change régulièrement.</p>

<p>En mettant à jour votre nom de domaine en DNS Dynamique dès que votre adresse IP change, cela permet d'avoir un temps d'indisponibilité assez court, à n'importe quel moment. En plus, ça se fait souvent en automatique.</p>

<p>C'est un système un peu batard né de l'utilisation du NAT et des IP privées, et nécessaire à cause des FAI trouvant plus simple de vous changer d'IP plutôt que de vous en filer une fixe. (On se demandera à jamais pourquoi)</p>

## Une bonne idée, mais...

<p>Bon, les définitions étant posées, parlons maintenant plus particulièrement du service d'OVH.</p>

<p>Proposer des DNS dynamiques, c'est bien, mais comme vu plus haut, ça nécessite une interaction côté client pour mettre à jour en temps réel (idéalement) l'entrée DNS.</p>

<p>Pour cela, OVH propose dans sa documentation divers logiciels à installer chez vous qui se chargent du boulot. Windows et Linux sont au rendez vous, mais malheureusement, la plupart de ces logiciels sont fait pour être installés sur votre PC perso, voir un serveur maison.</p>

<p>De plus, ils sont pour la plupart basés sur le fait que votre PC perso/votre serveur porte l'IP publique dynamique, ce qui est quand même très rare. (BiduleBox powaa !)</p>

<p>On peut s'en sortir avec un vérificateur d'adresse IP externe.</p>

<p>Mais la grosse limitation intervient lorsque l'on veut configurer cette mise à jour directement sur un routeur (accessoirement une BiduleBox). Ce qui est plus logique, puisque ce sont souvent eux qui portent l'IP publique dynamique.</p>

<p>Or, à moins que votre routeur propose directement de profiter du service OVH, vous allez devoir utiliser une URL HTTP pour faire cette mise à jour. Et ça, point de trace sur la documentation OVH, ni sur les forum, malgré quelques post courageux de personnes bien intentionnées.</p>

<p>&nbsp;</p>

## L'exemple du WRT54GL Tomato

<p>J'utilise pour ma part un routeur <a href="http://fr.wikipedia.org/wiki/WRT54GL">Linksys WRT54GL</a>, excellent produit, avec le firmware <a href="http://www.polarcloud.com/firmware">tomato</a>. Bien qu'assez riche en option pour configurer le DNS dynamique, ce firmware ne propose pas par défaut OVH. (Ni d'ailleurs aucun firmware WRT54GL à ma connaissance)</p>

<p>Ce firmware propose toutefois d'entrer une URL personnalisée pour aller mettre à jour votre enregistrement DNS. Plutôt sympathique, encore faut-il connaitre cette URL. Comme OVH n'en parle pas, il faut chercher.</p>

<p>Pour ma part, j'ai du regrouper plusieurs sites/blog en parlant pour réussir à obtenir une URL utilisable, je vous la fournit donc, après tout c'est l'info que vous cherchiez en venant ici&nbsp;;-)</p>

<p>Donc, dans le menu Basic -&gt; DDNS -&gt; Dynamic DNS 1 :</p>

<ul>
	<li>Choisir dans le menu déroulant Custom URL</li>
	<li>Entrer l'URL suivante : <u><strong>http://IDENTIFIANT:MOTDEPASSE@www.ovh.com/nic/update?system=dyndns&amp;hostname=NOMDEDOMAINE&amp;myip=@IP</strong></u></li>
</ul>

<p>Où :&nbsp;</p>

<ol>
	<li>IDENTIFIANT est l'identifiant de gestion du DNS dynamique configuré dans l'interface OVH</li>
	<li>MOTDEPASSE est le mot de passe de cet identifiant</li>
	<li>NOMDEDOMAINE est le nom de domaine à mettre à jour</li>
	<li>@IP est le mot clé de tomato pour récupérer la nouvelle IP publique</li>
</ol>

<p>N'oubliez pas de modifier dans le premier menu déroulant l'option que vous souhaitez :</p>

<ul>
	<li>WAN IP si votre routeur porte l'IP publique</li>
	<li>External IP checker si votre routeur est derrière une BiduleBox</li>
	<li>Custom IP si vous êtes en IP fixe, mais là l'intérêt est quand même moindre...</li>
</ul>

<p>Cochez "Force next update" pour que la mise à jour se fasse tout de suite, puis cliquez sur "Save".</p>

<p>La mise à jour va s'effectuer, et si tout va bien mettre à jour le DNS sans erreurs.</p>

<p>Voila, en espérant que ça serve à quelqu'un dans le meilleur des mondes&nbsp;</p>
