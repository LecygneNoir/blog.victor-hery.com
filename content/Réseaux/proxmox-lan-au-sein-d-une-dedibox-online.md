Title: Lan au sein d'une dedibox Online
subtitle: ; Proxmox
Date: 2012-09-12 20:30
Modified: 2012-09-12 20:30
Category: Réseaux
Tags: proxmox, debian, iptables, linux, dedibox online
keywords: proxmox, debian, iptables, linux, dedibox online
Slug: proxmox-lan-au-sein-d-une-dedibox-online
Authors: Victor
Status: published

[TOC]

## Problématique : faut qu'ça cause
<p>
	Ce tutoriel a été rédigé sur le modèle d'un serveur dédié "dedibox" loué par <a href="http://www.online.net">Online.net</a>.</p>
<p>
	<em>Nota : cet article est la transcription un peu plus détaillée et technique d'un post de moi-même sur le forum d'Online. Il n'y a donc pas de probème de droits :)</em></p>
<p>
	Cependant, il peut s'adapter à n'importe quel serveur ayant une distribution proxmox d'installée, au prix du changement de nom de quelques interfaces.</p>
<p>
	Globalement le problème se résume ainsi :</p>
<ul>
	<li>
		Votre proxmox est installé sur un serveur dédié</li>
	<li>
		Vous avez plusieurs machines virtuelles, qui ont besoin de causer entre elles, ou mieux avec l'extérieur (mises à jour, tout ça...)</li>
	<li>
		Votre herbergeur<a href="http://blog.héry.com/article12/on-rale-marre-du-filtrage-securite-des-hebergeurs"> <strike>est un gros extrémiste</strike> sécurise son réseau</a> et il coupe tout accès à votre serveur si vous avez le malheur de sortir sur le réseau avec une adresse IP/MAC privée</li>
</ul>
<p>
	&nbsp;</p>
<p>
	Il va donc falloir mettre en place un réseau virtuel interne à votre proxmox, et le cas échéant un système de NAT efficace pour permettre à vos VM avec adresses IP privées de sortir sur Internet.</p>
<p>
	<strong>Que ce soit clair, ces règles de pare-feu ne sécurisent en rien votre serveur, elles se contentent uniquement de permettre à vos machines virtuelles de causer entre elles.</strong></p>
## Posons les termes : de quoi on parle
<p>
	Pour rendre plus simples les explications suivantes, je vais poser dès maintenant la configuration que l'on va utiliser.</p>
<p>
	Le tutoriel se base sur proxmox V2, configuré juste après installation.</p>
<p>
	Dans ce cadre :</p>
<ol>
	<li>
		On utilise une distribution Proxmox, donc basée sur Debian</li>
	<li>
		vmbr0 est l'interface bridge créée par proxmox, normalement liée à eth0</li>
	<li>
		vmbr1 est une interface bridge supplémentaire créé par nos soins, sans nécessité d'être liée à une interface physique (éventuellement créée depuis l'interface web proxmox)</li>
	<li>
		Le pare-feu que nous utiliserons sera iptables</li>
	<li>
		Nous configurerons des containeurs OpenVZ (mais la configuration fonctionne avec des KVM en réseau bridgé)</li>
	<li>
		Une machine VMID OpenVZ existe déjà, et elle dispose d'une interface publique avec une adresse MAC générée via l'interface web Online. (hors cadre de ce tuto)</li>
	<li>
		Vos VM utilisent la plage d'adresse IP privées 192.168.0.0/16</li>
	<li>
		Vous possédez déjà quelques connaissances en ligne de commande/iptables</li>
</ol>
## Les mains dans le cambouis
<p>
	Première chose à faire, se connecter en SSH sur votre hyperviseur proxmox.</p>
<p>
	Utilisez Putty si vous êtes sous Windows, ou le shell via une applet java dans l'interface Proxmox en alternative.</p>
<p>
	Dans tous les cas, continuez une fois que vous avez un shell d'ouvert sur votre hyperviseur&nbsp; :-)</p>
### Evitons les blocages intempestifs
<p>
	La première manip va être de mettre en place un NAT sur l'interface physique qui sert à l'hyperviseur pour joindre Internet. Ceci permettra tout de suite d'éviter la moindre fuite d'un paquet venant d'une VM vers l'extérieur.</p>
<p>
	En effet, cette fuite pourrait entrainer la coupure de l'accès à votre serveur pour une durée indéterminée, évitons donc tout de suite ce cas là.</p>
<p>
	Un effet de bord intéressant est que vos futures VM pourront accéder à Internet à l'aide de ce NAT à l'aide d'un peu de routage ! Pratique pour les mises à jour.</p>
<p>
	Pour mettre en place ce NAT, utilisez les lignes suivantes (à utiliser en root, ou alternativement avec sudo) :</p>
<p>

<pre class="brush:bash;">
 $ iptables -t nat -A POSTROUTING -o vmbr0 -s 192.168.0.0/16 ! -d 192.168.0.0/16 -j MASQUERADE
 $ echo 1 &gt; /proc/sys/net/ipv4/ip_forward
</pre>

<p>
	Cela va permettre d'activer l'ip forwarding sur votre interface, et va faire en sorte que toutes vos VM en 192.168.0.0/16 seront nattées lorsqu'elles essaieront de joindre une machine qui n'est pas en 192.168.0.0/16.</p>
<p>
	<strong>Attention, changez les adresses IP selon votre configuration.</strong></p>
<p>
	Cette configuration est temporaire, elle n'aura plus d'effet au prochain redémarrage. Nous verrons dans la <a href="#Modifications permanentes">dernière partie</a> comment les rendre permanentes, mais vérifiez que tout fonctionne avant de faire ça.</p>
<p>
	(Si vous vous plantez et que vous bloquez l'accès à votre système, au moins un redémarrage règle le problème. Une fois les règles permanentes, vous êtes obligés de tout formater...)</p>
### Configuration OpenVZ
<p>
	Nous allons rajouter à la VM une interface réseau qui va bien. C'est également possible de le faire via l'interface web, mais comme on est en SSH on va pas s'embêter à ouvrir un navigateur !</p>
<p>
<pre class="brush:bash;">
vzctl set VMID --netif_add eth1,,,,vmbr1 --save
</pre>

<p>
	Ceci va permettre de rajouter à la machine VMID une interface eth1, branchée sur vmbr1. L'adresse MAC sera générée automatiquement. Le --save rend la modification permanente même après redémarrage de la VM.</p>
<p>
	Pour faire plus simple, vous pouvez mettre à vmbr1 une adresse IP dans la même plage que les VM (ici 192.168.0.0/16), histoire que l'hyperviseur puisse leur causer aussi. Ce n'est pas obligatoire ceci dit.</p>
<p>
	Pour faire ça, il suffit de modifier le fichier /etc/network/interfaces -&gt; Voir dans la section "<a href="#Modifications permanentes">Modification permamentes</a>"</p>
<p>
	Ensuite, il vous suffit de créer autant de VM que vous le souhaitez en réseau local. La seule contrainte est de leur mettre une interface réseau branchée sur vmbr1 à la création, puis une adresse IP dans la plage qui va bien.</p>
<p>
	De cette façon vous êtes parés.</p>
## <a name="Modifications permanentes">Modifications permanentes</a>
<p>
	Bon aller, pour que tout cela soit permanent même après redémarrage, voici les choses à rajouter.</p>
<p>
	Les modifications sont à faire sur l'hyperviseur.</p>
<p>
	Dans /etc/network/interfaces, rajoutez sous la section vmbr0 les lignes suivantes :</p>
<p>
<pre class="brush:bash;">
echo 1 &gt; /proc/sys/net/ipv4/ip_forward
post-up iptables -t nat -A POSTROUTING -o vmbr0 -s 192.168.0.0/16 ! -d 192.168.0.0/16 -j MASQUERADE
post-down iptables -t nat -D POSTROUTING -o vmbr0 -s 192.168.0.0/16 ! -d 192.168.0.0/16 -j MASQUERADE
</pre>

<p>
	De cette façon, le NAT sera configuré au démarrage sur vmbr0.</p>
<p>
	Pour avoir une adresse IP privée sur vmbr1, soit vous passez par l'interface web, soit vous retournez dans /etc/network/interfaces et vous rajouter dans vmbr1 les lignes qui vont bien (en vous basant sur vmbr0 par exemple)</p>
<p>
<pre class="brush:bash;">
addresse 192.168.0.1
netmask 255.255.0.0
broadcast 192.168.0.255
</pre>

<p>
	&nbsp;</p>
<p>
	Voila, j'espère que ce petit tuto vous aura été utile. Si vous avez des questions particulières, n'hésitez pas&nbsp; :-)</p>
<p>
	&nbsp;</p>
