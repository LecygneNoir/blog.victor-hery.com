Title: Culture et jardin potager en lasagne
Date: 2016-08-08 10:27
Modified: 2016-08-08 10:27
Category: Jardin
Tags: culture,lasagne,do it yourself,jardinage
keywords: culture,lasagne,do it yourself,jardinage
Slug: culture-jardin-potager-lasagnes
Authors: Victor
Status: published

[TOC]

## De quoi donc ?

<p>Aujourd'hui pas de jeux de mots, nous allons réellement discuter jardin, et mettre la main dans la terre au sens propre :-)</p>

<p>Dans le sympathique nouveau coin ou j'habite, j'ai la chance d'avoir un jardin, et comme j'ai toujours eu envie d'avoir un jardin potager, je me suis intéressé aux différents types de cultures.</p>

<p>Là aussi il s'agit de do it yourself, et de l'auto production ^^</p>

<p>Parmi le jardinage classique et les culture en carré, une technique m'a bien plu et permet un rendement optimal, c'est la <a href="https://fr.wikipedia.org/wiki/Jardinage_en_lasagnes">culture en lasagne</a>.</p>

<p>Après m'y être essayé avec pas mal de succès l'année dernière, j'ai décidé d'augmenter ma surface cette année, et j'en ai profité pour faire quelques photos afin de rédiger un petit article, qui j'espère plaira !</p>

## Concept

<p>La culture en lasagne fonctionne de manière très simple, et présente plusieurs avantages, notamment par rapport au jardinage classique (et quelques un par rapport au jardinage en carré)</p>

<p>Vous pouvez jardiner en lasagne n'importe où, même sur du béton ou du bitume. Un balcon par exemple est tout à fait envisageable !</p>

<p>Si vous jardinez sur de l'herbe ou des mauvaise herbes, pas besoin de désherber, vous posez votre lasagne par dessus et les mauvaises herbes seront éttoufées toutes seules.</p>

<p>Les différentes couches de matières (d'où le nom lasagne) fournissent tout ce qu'il faut aux plantes pour pousser de manière très exubérante. Légumes et/ou fleurs, selon les gouts ;)</p>

<p>Aucun besoin de désherber même durant l'année, car vous amenez de la terre "propre" sans mauvaises herbes, et en paillant régulièrement la lasagne, peu de mauvaises herbes arriveront à prendre. En plus, le paillage nourrira la lasagne.</p>

<p>Les différentes couches permettent de retenir efficacement l'eau, et donc de diminuer l'arrosage par rapport à de la pleine terre.</p>

<p>&nbsp;</p>

## Ingrédients et recette

<p>Il vous faut de la matière première de différente sorte.</p>

<p>Nécessaire :</p>

<p>&nbsp; - De quoi couvrir une surface de environ 1m sur 2m de carton d'emballage. <strong>Il faut du carton sans étiquettes, scotch, ou autre addictif chimiques de ce genre</strong>. Le carton ondulé est un must car il garde très bien l'humidité et réduit l'arrosage. Du carton duquel vous enlevez les étiquettes et le scotch fait très bien le job. Je vous conseille de planter vos tuteurs tout de suite, car une fois toutes les couches en place, c'est plus compliqué à enfoncer à travers ;-)</p>

<p><img alt="Lasagnes avec tuteurs" src="/images/Jardin/img_1303.jpg" style="width: 500px; height: 375px;" /></p>

<p>&nbsp; - Du résidu "brun" épais, qui permettra de faire une couche stable pour votre lasagne. Des copeaux de bois ou du branchage léger (type fagot) sont idéals</p>

<p>&nbsp; - Du résidu vert épais pour faire la première couche de vert. Des feuilles fraîches (je prends des feuilles de bambous) c'est parfait.</p>

<p><img alt="Lasagnes couches de gros" src="/images/Jardin/img_1306.jpg" style="width: 500px; height: 375px;" /></p>

<p>&nbsp; - Du résidu brun fin (par exemple du foin ou de la paille). Pas la peine de prendre du haut de gamme (en général assez cher) qui sert à nourrir les animaux. Ici, on veut plutôt du déchet :)</p>

<p>&nbsp; - Du résidu vert fin pour terminer vos couches. Ici, de l'herbe fraîchement coupée ou des déchets verts sont parfaits. <strong>Attention dans le cas de l'herbe coupée</strong>, elle composte à très haute température (jusqu'à 50°C), il faudra donc attendre au moins une semaine pour planter des légumes dans votre lasagne sinon les racines vont cramer et se dessécher. . . Par contre, la plupart des fruitiers s'en fichent (les fraisiers sont très content les pieds dans le chaud ;) )</p>

<p><img alt="" src="/images/Jardin/img_1312.jpg" style="width: 500px; height: 375px;" /></p>

<p>&nbsp; - Enfin, du compost bien avancé si vous en avez, sinon, du terreau pour plantation bio, ça s'achète par sac de 5 ou 10L en magasin et c'est parfait. Il permettra de planter vos plants dans quelques chose de solide, tout en évitant que les couches ne s'envolent avec le vent. Les couches quand à elles vont composter à différentes vitesses et apporter des nutriments en permanences à vos plantations.</p>

<p><img alt="" src="/images/Jardin/img_1310.jpg" style="width: 500px; height: 375px;" /></p>

<p>&nbsp; - Enfin, il est nécessaire d'arroser abondamment pour que la terre soit humide avant de planter, et également humidifier les différentes couches. L'avantage est que le carton retiendra l'eau, vous pouvez donc y aller sans problèmes.</p>

## Et on en fait quoi ?

<p>Et voila, vous pouvez planter ! Comme dit précedemment, mieux vaut attendre une petite semaine si vous avez utilisé de l'herbe fraiche le temps qu'elle composte. Sinon, vous pouvez directement planter !</p>

<p>Voici quelques conseils tirés de mon expérience (limitée je le précise). Si vous en avez d'autres ou souhaitez partager, n'hésitez pas à commenter !</p>

<p>&nbsp; - Si vous attendez avant de planter, n'oublier pas d'arroser abondamment juste après avoir planté pour aider vos plants à prendre leurs marques.</p>

<p>&nbsp; - Le type de plants dépendra de vos envies, et de la période de l'année. Avril-Juin est parfait pour les légumes d'été (courgettes, tomates, melon, concombres, etc). Les fruitiers peuvent être plantés à peu près n'importe quand (fraisier, framboises, ...). Idem pour certaines fleurs. Référez vous à vos plants pour en savoir plus !</p>

<p>&nbsp; - Pour la sélection, préférez des choses qui poussent en hauteur. Malheureusement l'inconvénient de la lasagne est que niveau profondeur, c'est pas trop ça. Du coup, tout ce qui est légumes à racines (poireau, radis, pommes de terres, ...) ne poussent pas idéalement. La racine a tendance à ramper plutôt que s'enfoncer, et c'est pas top.</p>

<p>&nbsp; - Si vous plantez des graines, mieux vaut laisser la terre exposée pour ne pas les stresser.</p>

<p>&nbsp; - Si par contre vous partez de plants déjà bien germés, vous pouvez recouvrir votre terre d'une nouvelle couche d'herbe ou de copeau pour limiter les arrivées de mauvaises herbes (notamment les graines volantes) et limiter encore l'arrosage (en limitant l'évaporation de la terre)</p>

<p>&nbsp; - Vous pouvez découper le carton qui dépasse pour faire plus joli, mais essayez de laisser au moins 3 à 5 cm autour de la lasagne, cela empêchera les mauvaises herbes grimpantes alentours de se reprendre dans la lasagne. Le carton disparaitra tout seul au bout de quelques mois, normalement d'ici là la lasagne sera bien posée.</p>

<p>&nbsp; - Il n'y a pas de miracles. La lasagne devrait permettre d'avoir un rendement très efficace (ça pousse très fort) et de limiter tout le travail de la terre (désherbage, creuser des trancher, etc) mais par contre, il faut entretenir ce qui pousse. De l'arrosage quotidien (ça dépend de ce qui est planté) et un entretien (tuteurage, enlever les gourmands des tomates, ...) est nécessaire ! Attention également aux maladies.</p>

<p>Et voila c'est tout. Je rajouterai des nouveaux conseils si j'en découvre d'autres :-)</p>

<p>N'hésitez pas à poster vos commentaires à propos de cette technique de jardinage et partagez vos expériences !</p>

## Le résultat,

<p>Voici quelques photos du résultat 2 mois après avoir planté environ.</p>

<p>Au premier plan, 4 pieds de tomates, et derrières 2 pieds de concombre. On voit sur la droite une 2eme lasagne avec une courgette. On voit qu'ils ont pris leurs aises :-)</p>

<p>Je vous mets aussi une photo de la récolte (d'une journée !). Les tomates ont mal démarrées à cause des 5 semaines de pluie du printemps cette année (2016), mais les concombres ont adorés !</p>

<p><img alt="" src="/images/Jardin/img_1931.jpg" style="width: 400px; height: 300px;" /><img alt="" src="/images/Jardin/img_1932.jpg" style="width: 400px; height: 300px;" /></p>
